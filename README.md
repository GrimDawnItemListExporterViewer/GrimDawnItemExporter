Quick notes
==========

are not in this repo (gitignored) :
-----------------------------------

* generated classes / files by GrimDawnExporterGenerator project
* db/ folder in resources since it's property of crate entertainment
* lib/ , out/ , .. the usual


Also, there's no build stuff, no Ant, Gradle, or whatever. You just have to be me or to guess (not very hard) to know how to build. 
But hey, I don't excpect anyone to try to :)

More doc, and so on, should come later.