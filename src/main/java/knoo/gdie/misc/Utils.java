package knoo.gdie.misc;

import knoo.gdie.config.OutputFormat;
import knoo.gdie.file.decryptor.GDDecoder;
import knoo.gdie.log.GDIELogger;
import knoo.gdie.output.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by knoodrake on 03/04/2017.
 */
public class Utils {

    public static Class<? extends Outputator> getOutputatorFromFormat(OutputFormat outputFormat) {
        switch (outputFormat) {
            case HTML: return HTMLOutputator.class;
            case CSV: return CSVOutputator.class;
            case JSON: return JSONOutputator.class;
            case JSHTML: return JSHTMLOutputator.class;
            case BBCODE: return BBCodeOutputator.class;
            case TEXT:
            default:
                return TextOutputator.class;
        }
    }

    public static String UpperCaseFirstLetter(String str) {
        return Character.toUpperCase(str.charAt(0)) + str.substring(1).toLowerCase();
    }


    public static String padRight(String s, int n) {
        if(s.length() == n) return s;
        if(s.length() > n) s = s.substring(0, n);
        return String.format("%1$-" + n + "s", s);
    }

    public static List<Integer> csvStrToIntegerList(final String csvStr) {
        if(csvStr.isEmpty()) return Collections.emptyList();
        return Arrays.asList(csvStr.split(",")).stream()
            .map(String::trim)
            .map(Integer::parseInt)
            .collect(Collectors.toList());
    }

    public static List<String> csvStrToLCaseStringList(final String csvStr) {
        if(csvStr.isEmpty()) return Collections.emptyList();
        return Arrays.asList(csvStr.split(",")).stream()
                .map(String::trim)
                .map(String::toLowerCase)
                .collect(Collectors.toList());
    }
}
