package knoo.gdie.misc;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by knoodrake on 20/04/2017.
 *
 * First try to guess steam install.
 * Then Game install.
 * Then try to find the most recently used (files being modified) save location
 * between the game install dirs' /save and the (multiple) steam cloud's /save paths.
 *
 * TODO GoG Install
 */
public class GameFileFinder {

    private static final String GD_STEAM_ID = "219990";

    public static String findSaves(){
        String steamInstallPath = findSteamInstallPath();
        if(steamInstallPath == null) return null;

        String steamGDInstallPath = findSteamGDInstallPath();
        List<String> authorizedDeviceID = findAuthorizedDeviceID();
        if(steamGDInstallPath == null && (authorizedDeviceID == null || authorizedDeviceID.isEmpty()))
            return null;

        List<String> savePathFound = new ArrayList<>();

        String steamNonCloudSavePath = steamGDInstallPath + "/save";
        if(isAValidSaveDirectory(steamNonCloudSavePath))
            savePathFound.add(steamNonCloudSavePath);

        authorizedDeviceID.forEach(id -> {
            String steamCloudSavePath = steamInstallPath + "/userdata/" + id + "/" + GD_STEAM_ID + "/remote/save";
             if(isAValidSaveDirectory(steamCloudSavePath)) {
                 savePathFound.add(steamCloudSavePath);
             }
        });

        if(savePathFound.isEmpty()) return null;
        if(savePathFound.size() == 1) return savePathFound.get(0);

        return getMostRecentlyUsedSaveLocation(savePathFound);
    }

    public static String findGameDBFile() {
        File steamGDInstallPath = new File(findSteamGDInstallPath());

        if(steamGDInstallPath.exists() && steamGDInstallPath.isDirectory() && steamGDInstallPath.canRead()) {
            File file = new File(steamGDInstallPath.getAbsolutePath() + "\\database\\database.arz");
            if(file.exists() && file.canRead())
                return file.getAbsolutePath();
        }

        return null;
    }

    private static String getMostRecentlyUsedSaveLocation(List<String> savePaths) {
        long mostRecentTimestamp = 0;
        String correspondigPath = null;

        for (String savePath : savePaths) {
            long mostRecentTimestampRecurse = findMostRecentTimestampRecurse(new File(savePath));
            if(mostRecentTimestampRecurse > mostRecentTimestamp) {
                mostRecentTimestamp = mostRecentTimestampRecurse;
                correspondigPath = savePath;
            }
        }
        return correspondigPath;
    }

    private static long findMostRecentTimestampRecurse(File path) {
        if(!path.canRead()) return 0;
        if(!path.isDirectory()) return path.lastModified();
        File[] files = path.listFiles();
        long mostRecentTS = 0;
        for (File file : files) {
            if(file.isDirectory()) {
                long mostRecentTimestampRecurse = findMostRecentTimestampRecurse(file);
                if (mostRecentTS < mostRecentTimestampRecurse) {
                    mostRecentTS = mostRecentTimestampRecurse;
                }
            } else {
                if(file.lastModified() > mostRecentTS)
                    mostRecentTS = file.lastModified();
            }
        }
        return mostRecentTS;
    }

    private static boolean isAValidSaveDirectory(String path) {
        File file = new File(path + "/transfer.gst");
        return file.exists();
    }

    private static List<String> findAuthorizedDeviceID() {
        List<String> strings = new ArrayList<>();
        try {
            strings.addAll(WinRegistry.subKeysForPath(WinRegistry.HKEY_CURRENT_USER, "SOFTWARE\\Valve\\Steam\\Users"));
        } catch (InvocationTargetException | IllegalAccessException e) {
        }
        return strings;
    }

    private static String findSteamInstallPath()  {
        try {
            String steamPath = WinRegistry.valueForKey(WinRegistry.HKEY_CURRENT_USER, "SOFTWARE\\Valve\\Steam", "SteamPath");
            if(steamPath != null) {
                File file = new File(steamPath);
                if(file != null && file.exists() && file.isDirectory())
                    return file.getAbsolutePath();
            }
        } catch (Exception e) {
        }
        return null;
    }

    private static String findSteamGDInstallPath() {
        try {
            String steamGDInstallPath = WinRegistry.valueForKey(WinRegistry.HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Steam App " + GD_STEAM_ID, "InstallLocation");
            if(steamGDInstallPath != null) {
                File file = new File(steamGDInstallPath);
                if(file != null && file.exists() && file.isDirectory())
                    return file.getAbsolutePath();
            }
        } catch (Exception e) {
        }
        return null;
    }
}
