package knoo.gdie.misc;

import com.dooapp.fxform.filter.ElementListFilter;
import com.dooapp.fxform.model.Element;
import knoo.gdie.Main;

import java.util.ArrayList;
import java.util.List;

public class HiddenSettingFieldFilter implements ElementListFilter {

    public List<Element> filter(List<Element> toFilter) {
        if(Main.getConfig().getSectionMainSettings().getShowAllSettings())
            return toFilter;

        List<Element> filtered = new ArrayList<>();
        for (Element field : toFilter) {
            if (field.getAnnotation(HiddenSetting.class) == null) {
                filtered.add(field);
            }
        }
        return filtered;
    }
}