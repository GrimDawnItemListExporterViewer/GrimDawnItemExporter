package knoo.gdie.log;

import javafx.event.EventHandler;
import javafx.scene.control.ScrollPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import knoo.gdie.Main;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.PrintStream;

/**
 * Created by knoodrake on 03/04/2017.
 */
public class GDIELogger extends ScrollPane {

    private final static String NL = System.getProperty("line.separator");
    private static GDIELogger _instance = null;
    private TextFlow textFlow;

    private GDIELogger(TextFlow textFlow) {
        super(textFlow);
        this.textFlow = textFlow;
        setFitToWidth(true);
    }

    public static GDIELogger get() {
        if(_instance == null) {
            TextFlow textFlow = new TextFlow();
            _instance = new GDIELogger(textFlow);
        }
        return _instance;
    }

    public interface ErrorHandler {
        void handle();
    }

    private ErrorHandler errorHandler = null;

    public void onError(ErrorHandler handler) {
        this.errorHandler = handler;
    }

    public enum Level {
        MSG(Color.BLACK),
        ERR(Color.RED),
        WARN(Color.ORANGE),
        DEBUG(Color.GRAY);

        private final Color color;

        Level(Color color) {
            this.color = color;
        }

        public Color getColor() {
            return color;
        }
    }

    public void log(final String msg, Level level) {
        StringBuilder sb = new StringBuilder();
        PrintStream console;
        switch (level) {
            case MSG:
                console = System.out;
                sb.append("  *  ");
                break;
            case ERR:
                console = System.err;
                sb.append("[ERR]");
                Toolkit.getDefaultToolkit().beep();
                if(errorHandler != null) errorHandler.handle();
                break;
            case WARN:
                console = System.err;
                sb.append(" !!! ");
                break;
            case DEBUG:
                console = System.out;
                if(!Main.getConfig().getSectionMainSettings().getDebug())
                    return;
                sb.append("{DBG}");
                break;
            default:
                console = System.err;
                sb.append(" ??? ");
                break;
        }
        sb.append(msg);
        sb.append(NL);

        String str = sb.toString();
        console.print(str);

        Text txt = new Text(str);
        txt.setFill(level.getColor());
        this.textFlow.getChildren().add(txt);
        layout();
        setVvalue(1.0);
    }

    public void log(Exception e) {
        logErr(e.getMessage());
        e.printStackTrace();
    }

    public void log(IOException e, String s) {
        logErr(s);
        logErr(e.getMessage());
        e.printStackTrace();
    }

    public void log(final String msg) {
        log(msg, Level.MSG);
    }

    public void logErr(final String msg) {
        log(msg, Level.ERR);
    }

    public void logDbg(final String msg) {
        log(msg, Level.DEBUG);
    }

    public void logWarn(final String msg) {
        log(msg, Level.WARN);
    }
}
