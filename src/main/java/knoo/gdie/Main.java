package knoo.gdie;

import com.dooapp.fxform.filter.ElementListFilter;
import com.dooapp.fxform.model.Element;
import com.dooapp.fxform.validation.FXFormValidator;
import com.dooapp.fxform.view.FXFormSkinFactory;
import com.dooapp.fxform.view.factory.impl.ParseErrorConstraintViolation;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import knoo.gdie.config.ConfigIniSection;
import knoo.gdie.config.generated.IniConfig;
import knoo.gdie.config.generated.IniConfigBean;
import knoo.gdie.log.GDIELogger;
import knoo.gdie.misc.GameFileFinder;
import knoo.gdie.misc.HiddenSettingFieldFilter;
import knoo.gdie.misc.Utils;
import knoo.gdie.recordsdb.DBFactory;
import knoo.gdie.sack.item.GDItem;
import knoo.gdie.sack.item.ItemNames;
import knoo.gdie.ui.webview.WebviewPanel;
import org.ini4j.Wini;
import org.kordamp.ikonli.javafx.FontIcon;

import javax.swing.*;
import javax.validation.ConstraintViolation;
import javax.validation.MessageInterpolator;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

/**
 * fonts:
 * http://aalmiray.github.io/ikonli/cheat-sheet-fontawesome.html
 */

public class Main extends Application {

    private final IniConfigBean config = new IniConfigBean();

    private static boolean IDE_MODE = false;
    private static Wini loadedIniFromFile;
    private static String configFilePath = null;

    private GameFilesReader gameFilesReader;
    private GameFilesFilter gameFilesFilter;
    private GameFilesOutputer gameFilesOutputer;

    /**
     * @param args
     * java args:
     *      - no parameter: will try to find and use a config.ini in cwd
     *      - loadConfigFromResources as 1st parameter to load the config.ini from classpath
     *      - any other 1st parameter is considered the config.ini to be used
     *      - an additional parameter "-noUI" to skip the GUI and silently output.
     */
    public static void main(String[] args) throws InterruptedException {
        List<String> argsList = Arrays.asList(args);

        final CountDownLatch latch = new CountDownLatch(1);
        SwingUtilities.invokeLater(() -> {
            new JFXPanel(); // initializes JavaFX environment
            latch.countDown();
        });
        latch.await();
//  TODO WIP, testing
//        SaveFileFinder svf = new SaveFileFinder();
//        svf.findThoseF_ckingSaveFiles();

        loadedIniFromFile = new Wini();

        try {
            if(!argsList.isEmpty() && "loadConfigFromResources".equals(argsList.get(0))) {
                IDE_MODE = true;
                loadedIniFromFile.load(ClassLoader.class.getResourceAsStream("/dev_config.ini"));
            } else {
                if(!argsList.isEmpty()) {
                    configFilePath = argsList.get(0);
                    loadedIniFromFile.load(new File(configFilePath));
                } else {
                    String cwd = Paths.get(".").toAbsolutePath().normalize().toString();
                    configFilePath = cwd + "\\config.ini";
                    loadedIniFromFile.load(new File(configFilePath));
                }
            }
        } catch (IOException e) {
            GDIELogger.get().logErr("No valid configuration file found");
            configFilePath = null;
        }

        boolean[] noUI = new boolean[] {false};
        argsList.forEach(arg -> {
            if("-noUI".equalsIgnoreCase(arg))
                noUI[0] = true;
        });

            if (noUI[0]) {
                Main app = new Main();
                try {
                    app.init();
                    List<GDItem> filteredItems = app.gameFilesFilter.filter(app.gameFilesReader.getItems());
                    if (!filteredItems.isEmpty())
                        app.gameFilesOutputer.output(filteredItems);
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                launch(args);
            }
    }

    @Override
    public void init() throws Exception {
        super.init();

        __instance = this;

        config.initFrom(loadedIniFromFile);
        config.getSections().forEach(section -> {
            // TODO de-escape newlines
        });

        ItemNames.init();
        DBFactory.getDB().init("/db/");

        gameFilesReader = new GameFilesReader();
        gameFilesFilter = new GameFilesFilter();
        gameFilesOutputer = new GameFilesOutputer();

        if(getConfig().getSectionMainSettings().getSavePath().isEmpty()) {
            String saveLocation = GameFileFinder.findSaves();
            if(saveLocation != null) {
                GDIELogger.get().log("Looking for game save files location. Found a good candidate: " + saveLocation);
                getConfig().getSectionMainSettings().setSavePath(saveLocation);
            }
        }

        tryToReadGameFiles();
    }

    public void tryToReadGameFiles() {
        try {
            gameFilesReader.read();
        } catch (IOException e) {
            GDIELogger.get().logErr("unable to read save files. Pleas make sure the Save Location (savePath in the ini) is correct and readable.");
        }
    }

    private static Main __instance = null;
    public static Main get() {
        return __instance;
    }

    public static IniConfig getConfig() {
        return __instance.config;
    }

    public void loadEmbededOutput(final String contentStr) {
        if(browserTab == null) {
            browser = new WebviewPanel();
            browserTab = new Tab("Output");
            browserTab.setGraphic(new FontIcon("fa-list-ul"));
            browserTab.setContent(browser);
            browserTab.setClosable(true);
            browserTab.getStyleClass().add("output-tab");
        }
        if(!content.getTabs().contains(browserTab)) {
            content.getTabs().add(browserTab);
        }
        browser.loadContent(contentStr);
        content.getSelectionModel().select(browserTab);
    }

    private WebviewPanel browser = null;
    private TabPane content = null;
    private Tab browserTab = null;

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("GrimDawn Items List Exporter");
        primaryStage.getIcons().add(new Image("/icon.png"));

        //FIXME
//        FXFormValidator fakeValidator = getFXFormValidator();

        VBox root = new VBox();
        content = new TabPane();
        primaryStage.setResizable(false);

        HiddenSettingFieldFilter hiddenSettingFilter = new HiddenSettingFieldFilter();

        BorderPane borderPane = new BorderPane(root);
        BorderPane borderPane2 = new BorderPane();
        Scene scene = new Scene(borderPane, 1280, 720);
        Scene scene2 = new Scene(borderPane2, 1280, 580);
        String css = this.getClass().getResource("/ui.css").toExternalForm();
        scene.getStylesheets().add(css);
        scene2.getStylesheets().add(css);
        config.getSectionsAsFXForms().forEach(sectionFXForm -> {

            sectionFXForm.addFilters((ElementListFilter)hiddenSettingFilter);

//            sectionFXForm.setFxFormValidator(fakeValidator);
            sectionFXForm.setSkin(FXFormSkinFactory.INLINE_FACTORY.createSkin(sectionFXForm));
            sectionFXForm.setPrefWidth(500);
            ConfigIniSection sourceSection = (ConfigIniSection) sectionFXForm.getSource();

            Tab tab = new Tab(Utils.UpperCaseFirstLetter(sourceSection.getName()));
            if(sourceSection.getIconName() != null) {
                tab.setGraphic(new FontIcon(sourceSection.getIconName()));
            }
            ScrollPane scroller = new ScrollPane(content);
            scroller.setFitToWidth(true);
            scroller.setFitToHeight(true);
            scroller.setContent(sectionFXForm);
            tab.setContent(scroller);
            tab.setClosable(false);

            content.getTabs().add(tab);
        });

        // Bottom buttons

        HBox bottomButtons = new HBox();
        Button generateBtn = new Button("Generate output", new FontIcon("fa-list-ul"));
        generateBtn.setId("generate-btn");
        Button reReadGameFilesBtn = new Button("re-read game files", new FontIcon("fa-refresh"));
        reReadGameFilesBtn.setTooltip(new Tooltip("Update your items (if you are playing right now for instance)"));
        Button saveConfigIniFileBtn = new Button("Save config file", new FontIcon("fa-save"));
        saveConfigIniFileBtn.setTooltip(new Tooltip("Saves the current settings to the current config file (default config.ini)"));
        Button toggleLogConsole = new Button("Log", new FontIcon("fa-newspaper-o"));
        toggleLogConsole.setTooltip(new Tooltip("shows/hide the log console)"));
        bottomButtons.getChildren().add(generateBtn);
        bottomButtons.getChildren().add(reReadGameFilesBtn);
        bottomButtons.getChildren().add(saveConfigIniFileBtn);
        bottomButtons.getChildren().add(toggleLogConsole);
        generateBtn.setDefaultButton(true);
        generateBtn.setOnAction(event -> {
            if(!gameFilesReader.haveRead()) {
                tryToReadGameFiles();
            }
            List<GDItem> filteredItems = gameFilesFilter.filter(gameFilesReader.getItems());
            if(!filteredItems.isEmpty())
                gameFilesOutputer.output(filteredItems);
        });

        reReadGameFilesBtn.setOnAction(event -> {
            gameFilesReader.clear();
            tryToReadGameFiles();
        });

        saveConfigIniFileBtn.setOnAction(event -> {
            if(IDE_MODE) return;
            if(configFilePath == null)
                configFilePath = "config.ini";

            File configFile = new File(configFilePath);

            Wini wini = new Wini();
            config.getSections().forEach(section -> wini.add(section.getIniName()).from(section) );
            wini.values().forEach(section -> {
                section.remove("_pcSupport");
                section.remove("_vcSupport");
                section.remove("name");
                section.remove("iconName");
                section.remove("iniName");
                section.forEach((key, value) -> {
                    if(value.contains("\n")) {
                        section.put(key, value.replaceAll("\n", "\\\\n"));
                    }
                });
            });

            try {
                wini.store(configFile);
                GDIELogger.get().log("Configuration file written: " + configFile.getAbsolutePath());
            } catch (IOException e) {
                GDIELogger.get().log(e, "unable to write the configuration file.");
            }
        });


        // Log textarea
        GDIELogger logArea = GDIELogger.get();
        logArea.setMinHeight(150);
        logArea.setMaxHeight(150);

        toggleLogConsole.setOnAction(e -> {
            if(logArea.isVisible()) {
                logArea.setVisible(false);
                primaryStage.setScene(scene2);
                borderPane.getChildren().remove(root);
                borderPane2.getChildren().add(root);
            } else {
                logArea.setVisible(true);
                primaryStage.setScene(scene);
                borderPane2.getChildren().remove(root);
                borderPane.getChildren().add(root);
            }

        });

//        root.getChildren().add(scroller);
        root.getChildren().add(content);
        root.getChildren().add(bottomButtons);
        root.getChildren().add(logArea);

        primaryStage.setScene(scene);
        primaryStage.setOnShown(e -> toggleLogConsole.fire());
        logArea.onError(() -> {if(!logArea.isVisible()) { toggleLogConsole.fire();}});
        primaryStage.show();

    }

    private FXFormValidator getFXFormValidator() {
        return new FXFormValidator() {
            @Override
            public List<ConstraintViolation> validate(Element element, Object newValue, Class... groups) {
//                return Collections.emptyList();
                List<ConstraintViolation> constraintViolations = new ArrayList<>();
                constraintViolations.add(new ParseErrorConstraintViolation(newValue.toString()));
                return constraintViolations;
            }

            @Override
            public List<ConstraintViolation> validateClassConstraint(Object bean) {
                return Collections.emptyList();
            }

            @Override
            public MessageInterpolator getMessageInterpolator() {
                return new MessageInterpolator() {
                    @Override
                    public String interpolate(String messageTemplate, Context context) {
                        return messageTemplate;
                    }

                    @Override
                    public String interpolate(String messageTemplate, Context context, Locale locale) {
                        return messageTemplate;
                    }
                };
            }
        };
    }

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        return map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(/*Collections.reverseOrder()*/))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (e1, e2) -> e1,
                        LinkedHashMap::new
                ));
    }
}
