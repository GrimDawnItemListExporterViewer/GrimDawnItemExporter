package knoo.gdie;

import knoo.gdie.config.OutputFormat;
import knoo.gdie.log.GDIELogger;
import knoo.gdie.misc.Utils;
import knoo.gdie.output.Outputator;
import knoo.gdie.output.TextOutputator;
import knoo.gdie.sack.character.GDCharacter;
import knoo.gdie.sack.item.GDItem;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by knoodrake on 03/04/2017.
 */
public class GameFilesOutputer {

    public void output(List<GDItem> filteredLegendaries) {
        //TODO to sort
        // Map<GDItem, Long> sortedItemMapByQty = sortByValue(legendariesByQty);
        Map<GDItem, List<GDCharacter>> ownersByItems = new HashMap<>();
        HashMap<GDItem, Long> legendariesByQty = filteredLegendaries.stream().collect(
                Collectors.groupingBy(i -> {
                    if(!ownersByItems.containsKey(i))
                        ownersByItems.put(i, new ArrayList<>());
                    ownersByItems.get(i).add(i.getInitialOwner());
                    return i;
                }, HashMap::new, Collectors.counting())
        );



        // Judge which items are considarated in duplicate
        // Also put the names of all owners
        legendariesByQty.forEach((item, qty) -> {
            List<GDCharacter> gdCharacters = ownersByItems.get(item);
            if(gdCharacters != null) {
                item.setOwners(gdCharacters);
            }
            if(qty > 1) {
                item.setJuggedDuplicate(true);
            }
        });

        OutputFormat outputFormat = OutputFormat.getFromName(Main.getConfig().getSectionMainSettings().getOutputFormat().toLowerCase());
        String outputFileName = Main.getConfig().getSectionMainSettings().getOutputFile();

        if(!outputFileName.isEmpty() || Main.getConfig().getSectionMainSettings().getUseTemporaryFile()) {
            String output = null;
            Class<? extends Outputator> clazz = Utils.getOutputatorFromFormat(outputFormat);
            Outputator outputator = null;
            try {
                outputator = clazz.newInstance();
            } catch (Exception e) {
                GDIELogger.get().log(e);
            }
            output = outputator.outputStuff(legendariesByQty);
            if( Main.getConfig().getSectionMainSettings().getUseEmbededViewer()) {
                Main.get().loadEmbededOutput(output);
            } else if( Main.getConfig().getSectionMainSettings().getUseTemporaryFile()) {
                final String ext = outputFormat.getExtension();
                File temp = null;
                try {
                    temp = File.createTempFile("gdieTmpExport", ext);
                    temp.deleteOnExit();
                    try(  PrintWriter out = new PrintWriter(temp)  ){
                        out.println( output );
                        if(Main.getConfig().getSectionMainSettings().getAutoOpenFile()) {
                            Desktop.getDesktop().open(temp);
                        }
                        GDIELogger.get().log("file written to: " + outputFileName);
                    }
                } catch (IOException e) {
                    GDIELogger.get().log(e);
                }
            } else {
                try(  PrintWriter out = new PrintWriter(outputFileName)  ){
                    out.println( output );
                    if(Main.getConfig().getSectionMainSettings().getAutoOpenFile()) {
                        Desktop.getDesktop().open(new File(outputFileName));
                    }
                    GDIELogger.get().log("file written to: " + outputFileName);
                } catch (IOException e) {
                    GDIELogger.get().log(e);
                }
            }
        } else {
            TextOutputator textOutputator = new TextOutputator();
            GDIELogger.get().log(textOutputator.outputStuff(legendariesByQty));
        }
    }
}
