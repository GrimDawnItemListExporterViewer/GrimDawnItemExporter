package knoo.gdie.config;

import java.beans.PropertyChangeListener;
import java.beans.VetoableChangeListener;

public interface ConfigIniSection {

        String getName();

        String getIniName();

        String getIconName();

        void addPropertyChangeListener(String property, PropertyChangeListener listener);

        void addVetoableChangeListener(String property, VetoableChangeListener listener);

        void removePropertyChangeListener(String property, PropertyChangeListener listener);

        void removeVetoableChangeListener(String property, VetoableChangeListener listener);
    }