package knoo.gdie.config;

import com.dooapp.fxform.annotation.NonVisual;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;

public abstract class AbstractSectionBean implements ConfigIniSection {

        @NonVisual
        protected final String name;
        @NonVisual
        protected final String iniName;
        @NonVisual
        protected final String iconName;
        @NonVisual
        protected PropertyChangeSupport _pcSupport;
        @NonVisual
        protected VetoableChangeSupport _vcSupport;

        public AbstractSectionBean(final String name, final String iniName, final String iconName) {
            this.name = name;
            this.iniName = iniName;
            this.iconName = iconName;
            _pcSupport = new PropertyChangeSupport(this);
            _vcSupport = new VetoableChangeSupport(this);
        }

        @Override
        public String getName() {
            return this.name;
        }

        @Override
        public String getIniName() {
            return this.iniName;
        }

        @Override
        public String getIconName() {
            return this.iconName;
        }

        public void setName() {}

        public PropertyChangeSupport get_pcSupport() {
            return  _pcSupport;
        }

        public VetoableChangeSupport get_vcSupport() {
            return _vcSupport;
        }

        public void set_pcSupport(PropertyChangeSupport _pcSupport) {
            this._pcSupport = _pcSupport;
        }

        public void  set_vcSupport(VetoableChangeSupport _vcSupport) {
            this._vcSupport = _vcSupport;
        }

        @Override public void removePropertyChangeListener(String property, PropertyChangeListener listener)
        {
            _pcSupport.removePropertyChangeListener(property, listener);
        }

        @Override public void removeVetoableChangeListener(String property, VetoableChangeListener listener)
        {
            _vcSupport.removeVetoableChangeListener(property, listener);
        }

        @Override public void addPropertyChangeListener(String property, PropertyChangeListener listener)
        {
            _pcSupport.addPropertyChangeListener(property, listener);
        }

        @Override public void addVetoableChangeListener(String property, VetoableChangeListener listener)
        {
            _vcSupport.addVetoableChangeListener(property, listener);
        }

    }