package knoo.gdie.config;

import java.util.Arrays;

/**
 * Created by knoodrake on 03/04/2017.
 */
public enum OutputFormat {
    HTML("html", ".html"),
    CSV("csv", ".csv"),
    TEXT("text", ".txt"),
    BBCODE("bbcode", ".txt"),
    JSON("json", ".js"),
    JSHTML("jshtml",".html");

    private final String formatName;
    private final String extension;

    public String getFormatName() {
        return formatName;
    }

    public String getExtension() {
        return extension;
    }

    OutputFormat(String formatName, String extension) {
        this.formatName = formatName;
        this.extension = extension;
    }

    public static OutputFormat getFromName(String name) {
        final OutputFormat[] format = {OutputFormat.TEXT};
        Arrays.asList(OutputFormat.values()).forEach(f -> {
            if(f.getFormatName().equals(name)) {
                format[0] = f;
            }
        });
        return format[0];
    }
}