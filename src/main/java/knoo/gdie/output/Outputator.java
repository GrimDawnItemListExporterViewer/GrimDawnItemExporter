package knoo.gdie.output;

import knoo.gdie.sack.item.GDItem;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by knoodrake on 29/03/2017.
 */
public interface Outputator {

    String outputStuff(Map<GDItem, Long> legendariesByQty);
}
