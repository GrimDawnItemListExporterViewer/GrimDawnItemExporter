package knoo.gdie.output;

import knoo.gdie.Main;
import knoo.gdie.config.generated.IniConfig;
import knoo.gdie.log.GDIELogger;
import knoo.gdie.sack.item.GDItem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

/**
 * Created by knoodrake on 29/03/2017.
 */
public class HTMLOutputator implements Outputator {

    protected final static String NL = System.getProperty("line.separator");

    private final Boolean linkItems;
    private final String linkItemsItemPlaceholder;
    private final String linkItemsQuery;
    private final String stylesheetUri;
    private final String stylesheetEmbeed;
    private final Boolean onlyOutputTheList;
    private final Boolean noStyle;



    public HTMLOutputator() {
        IniConfig.SectionHtml htmlConfig = Main.getConfig().getSectionHtml();
        IniConfig.SectionMainSettings outputConfig = Main.getConfig().getSectionMainSettings();

        this.linkItems = htmlConfig.getLinkItems();
        this.linkItemsItemPlaceholder = outputConfig.getLinkItemsItemPlaceholder();
        this.linkItemsQuery = outputConfig.getLinkItemsQuery();
        this.stylesheetUri = htmlConfig.getStylesheetUri();
        this.stylesheetEmbeed = htmlConfig.getStylesheetEmbeed();
        this.onlyOutputTheList = htmlConfig.getOnlyOutputTheList();
        this.noStyle = htmlConfig.getNoStyle();
    }

    @Override
    public String outputStuff(Map<GDItem, Long> legendariesByQty) {
        StringBuilder sb = new StringBuilder();
        if(!Boolean.TRUE.equals(onlyOutputTheList)) {
            sb.append(generateHeader());
        }
        if(Main.getConfig().getSectionItems().getOutputNonDuplicate()) {
            sb.append(generateUniqueHeader());
            sb.append(generateUniqueList(legendariesByQty));
        }
        sb.append(generateDuplicateHeader());
        sb.append(generateDuplicateList(legendariesByQty));
        if(!Boolean.TRUE.equals(onlyOutputTheList)) {
            sb.append(generateFooter());
        }
        return sb.toString();
    }

    private static String escapeDQuote(String s) {
        return s.replaceAll("\"","&quot;");
    }
    private void getItemPropertiesAttributes(StringBuilder sb, GDItem item) {
        sb.append(" data-item-rarity=\"");
        sb.append(escapeDQuote(item.getRarity().name()));
        sb.append("\"");
        sb.append(" data-item-type=\"");
        sb.append(escapeDQuote(item.getGearType().getParent().name()));
        sb.append("\"");
        sb.append(" data-item-subtype=\"");
        sb.append(escapeDQuote(item.getGearType().name()));
        sb.append("\"");
        sb.append(" data-item-isPartOfASet=\"");
        sb.append(item.isPartOfASet() ? "true" : "false");
        sb.append("\"");
        if(item.isPartOfASet()) {
            sb.append(" data-item-set=\"");
            sb.append(escapeDQuote(item.getRarity().name()));
            sb.append("\"");
        }
    }

    private String generateDuplicateList(Map<GDItem, Long> legendariesByQty) {
        StringBuilder sb = new StringBuilder();
        sb.append("<ul class=\"duplicate_list\">");
        legendariesByQty.forEach((item, num) -> {
            if (item.isJuggedDuplicate()) {
                sb.append("<li ");
                getItemPropertiesAttributes(sb, item);
                sb.append(">");
                sb.append("<span class='num'>");
                sb.append(num);
                sb.append("</span><span class='item'>");
                sb.append(generateLegendaryLabel(item));
                sb.append("</span></li>");
            }
        });
        sb.append("</ul>");
        return sb.toString();
    }

    private String generateUniqueList(Map<GDItem, Long> legendariesByQty) {
        StringBuilder sb = new StringBuilder();
        sb.append("<ul class=\"uniq_list\">");
        legendariesByQty.forEach((item, num) -> {
            if (!item.isJuggedDuplicate()) {
                sb.append("<li ");
                getItemPropertiesAttributes(sb, item);
                sb.append(">");
                sb.append(generateLegendaryLabel(item));
                sb.append("</li>");
            }
        });
        sb.append("</ul>");
        return sb.toString();
    }

    private String generateLegendaryLabel(GDItem item) {
        if(Boolean.TRUE.equals(linkItems)) {
            if(linkItemsItemPlaceholder != null && linkItemsQuery != null) {
                try {
                    String uriSafeName = URLEncoder.encode(item.getCompleteNiceName(), "UTF-8");
                    String query = linkItemsQuery.replaceAll(linkItemsItemPlaceholder, uriSafeName);
                    return "<a href=\"" + query + "\">" + item.getCompleteNiceName() + "</a>";
                } catch (UnsupportedEncodingException e) {
                    GDIELogger.get().log(e);
                }
            }
        }
        return item.getCompleteNiceName();
    }


    private String generateDuplicateHeader() {
        return "<h3 class=\"duplicate_legendaries_title\">Duplicate Items</h3>";
    }


    private String generateUniqueHeader() {
        return "<h3 class=\"uniq_legendaries_title\">Unique Items</h3>";
    }

    private String generateFooter() {
        return "</div></body></html>";
    }

    private String generateHeader() {
        StringBuilder sb = new StringBuilder();
        sb.append("<HTML>");
        sb.append("<header>");
        sb.append("   <title>GD Items list</title>");
        if(!noStyle) {
            if(stylesheetUri.isEmpty() && stylesheetEmbeed.isEmpty()) {
                sb.append("<style type=\"text/css\" id=\"embeded_stylesheet\">");
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(ClassLoader.class.getResourceAsStream("/default_style.css")));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);                    }
                    reader.close();
                } catch (IOException e) {
                    GDIELogger.get().log(e);
                }
                sb.append("</style>");
                sb.append(NL);
            }
            if (!stylesheetUri.isEmpty()) {
                sb.append("<link href=\"");
                sb.append(stylesheetUri);
                sb.append("\" rel=\"stylesheet\" id=\"linked_stylesheet\" type=\"text/css\" >");
                sb.append(NL);
            }
            if (!stylesheetEmbeed.isEmpty()) {
                sb.append("<style type=\"text/css\" id=\"embeded_stylesheet\">");
                sb.append(NL);
                try {
                    Files.lines(Paths.get(stylesheetEmbeed)).forEach(l -> sb.append(l));
                } catch (IOException e) {
                    GDIELogger.get().log(e);
                }
                sb.append("</style>");
                sb.append(NL);
            }
        }
        sb.append("</header>");
        sb.append("<body>");
        sb.append("<div class=\"main\">");
        sb.append("<h1>GD Items Collection !</h1>");
        return sb.toString();
    }
}
