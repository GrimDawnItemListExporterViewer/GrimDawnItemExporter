package knoo.gdie.output;

import knoo.gdie.Main;
import knoo.gdie.config.generated.IniConfig;
import knoo.gdie.log.GDIELogger;
import knoo.gdie.sack.item.GDItem;
import knoo.gdie.sack.item.ItemRarity;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

/**
 * Created by knoodrake on 30/03/2017.
 */
public class BBCodeOutputator implements Outputator {

    protected final static String NL = System.getProperty("line.separator");

    private final Boolean linkItems;
    private final String linkItemsItemPlaceholder;
    private final String linkItemsQuery;
    private final Boolean mentionIfIsASet;
    private final Boolean mentionNumberOwned;
    private final Boolean useBBCodeList;
    private final String legendaryColorName;
    private final String epicColorName;
    private final String rareColorName;
    private final Boolean useColors;

    public BBCodeOutputator() {
        IniConfig.SectionForum bbCodeConfig = Main.getConfig().getSectionForum();
        IniConfig.SectionMainSettings outputConfig = Main.getConfig().getSectionMainSettings();

        this.linkItems = bbCodeConfig.getLinkItems();
        this.linkItemsItemPlaceholder = outputConfig.getLinkItemsItemPlaceholder();
        this.linkItemsQuery = outputConfig.getLinkItemsQuery();
        this.legendaryColorName = bbCodeConfig.getLegendaryColorName();
        this.epicColorName = bbCodeConfig.getEpicColorName();
        this.rareColorName = bbCodeConfig.getRareColorName();
        this.mentionIfIsASet = bbCodeConfig.getMentionIfIsASet();
        this.mentionNumberOwned = bbCodeConfig.getMentionNumberOwned();
        this.useBBCodeList= bbCodeConfig.getUseBBCodeList();
        this.useColors= bbCodeConfig.getUseColors();
    }

    @Override
    public String outputStuff(Map<GDItem, Long> legendariesByQty) {
        StringBuilder sb = new StringBuilder();

        if(Main.getConfig().getSectionItems().getOutputNonDuplicate()) {
            sb.append(generateUniqueHeader());
            sb.append(generateUniqueList(legendariesByQty));
        }
        if(Main.getConfig().getSectionItems().getOutputNonDuplicate())
            sb.append(generateDuplicateHeader());
        sb.append(generateDuplicateList(legendariesByQty));

        return sb.toString();
    }

    private String generateDuplicateList(Map<GDItem, Long> legendariesByQty) {
        StringBuilder sb = new StringBuilder();
        if(useBBCodeList) sb.append("[LIST]");
        legendariesByQty.forEach((item, num) -> {
            if (item.isJuggedDuplicate()) {
                if(useBBCodeList)
                    sb.append("[*]");
                if(mentionNumberOwned) {
                    sb.append("[B]");
                    sb.append(num);
                    sb.append("[/B] ");
                }
                sb.append(generateLegendaryLabel(item));
                if(mentionIfIsASet && item.isPartOfASet())
                    sb.append("  [I][SIZE=\"1\"]set[/SIZE][/I]");
                sb.append(NL);
            }
        });
        if(useBBCodeList) sb.append("[/LIST]");
        return sb.toString();
    }


    private String generateUniqueHeader() {
        return "[SIZE=\"4\"]Not Duplicates:[/SIZE]" + NL;
    }

    private String generateDuplicateHeader() {
        return "[SIZE=\"4\"]Duplicates:[/SIZE]" + NL;
    }

    private String generateUniqueList(Map<GDItem, Long> legendariesByQty) {
        StringBuilder sb = new StringBuilder();
        if(useBBCodeList) sb.append("[LIST]");
        legendariesByQty.forEach((item, num) -> {
            if (!item.isJuggedDuplicate()) {
                if(useBBCodeList)
                    sb.append("[*]");
                sb.append(generateLegendaryLabel(item));
                if(mentionIfIsASet && item.isPartOfASet())
                    sb.append("  [I][SIZE=\"1\"]set[/SIZE][/I]");
                sb.append(NL);
            }
        });
        if(useBBCodeList) sb.append("[/LIST]");
        return sb.toString();
    }

    private String generateLegendaryLabel(GDItem item) {
        if(Boolean.TRUE.equals(linkItems)) {
            if(linkItemsItemPlaceholder != null && linkItemsQuery != null) {
                String uriSafeName = null;
                try {
                    uriSafeName = URLEncoder.encode(item.getCompleteNiceName(), "UTF-8");
                    String query = linkItemsQuery.replaceAll(linkItemsItemPlaceholder, uriSafeName);
                    if(useColors) {
                        return String.format("[URL=\"%s\"][COLOR=\"%s\"]%s[/COLOR][/URL]", query, getItemColor(item), item.getCompleteNiceName());
                    } else {
                        return String.format("[URL=\"%s\"]%s[/URL]", query, item.getCompleteNiceName());
                    }
                } catch (UnsupportedEncodingException e) {
                    GDIELogger.get().log(e);
                }
            }
        }
        if(useColors) {
          return String.format("[COLOR=\"%s\"]%s[/COLOR]", getItemColor(item), item.getCompleteNiceName());
        }
        return item.getCompleteNiceName();
    }

    private String getItemColor(GDItem item) {
        if(ItemRarity.EPIC.equals(item.getRarity())) return epicColorName;
        if(ItemRarity.RARE.equals(item.getRarity())) return rareColorName;
        if(ItemRarity.LEGENDARY.equals(item.getRarity())) return legendaryColorName;
        return "";
    }
}
