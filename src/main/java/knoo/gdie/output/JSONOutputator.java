package knoo.gdie.output;

import knoo.gdie.misc.Utils;
import knoo.gdie.sack.item.GDItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by knoodrake on 04/04/2017.
 */
public class JSONOutputator implements Outputator {

    public class JSONGDItem {
        public String name;
        public String baseName;
        public String prefix;
        public String suffix;
        public Set<String> owners;
        public boolean isPartOfASet;
        public String itemSetName;
        public String rarity;
        public String baseItemType;
        public String ItemType;
        public Long qty;

        public String asJSONString() {
            StringBuilder sb = new StringBuilder();
            sb.append("{");
            sb.append("\"name\":");
            sb.append("\"");
            sb.append(escapeDoubleQuote(name));
            sb.append("\",");
            sb.append("\"prefix\":");
            sb.append("\"");
            sb.append(escapeDoubleQuote(prefix));
            sb.append("\",");
            sb.append("\"baseName\":");
            sb.append("\"");
            sb.append(escapeDoubleQuote(baseName));
            sb.append("\",");
            sb.append("\"suffix\":");
            sb.append("\"");
            sb.append(escapeDoubleQuote(suffix));
            sb.append("\",");
            sb.append("\"owners\":");
            sb.append("[");
            sb.append(String.join(",", owners.stream().map(
                    owner -> "\"" + escapeDoubleQuote(owner) + "\""
                ).collect(Collectors.toList())));
            sb.append("],");
            sb.append("\"isPartOfASet\":");
            sb.append(isPartOfASet ? "true" : "false");
            sb.append(",");
            sb.append("\"rarity\":");
            sb.append("\"");
            sb.append(rarity);
            sb.append("\",");
            sb.append("\"itemSetName\":");
            sb.append("\"");
            sb.append(escapeDoubleQuote(itemSetName));
            sb.append("\",");
            sb.append("\"baseItemType\":");
            sb.append("\"");
            sb.append(baseItemType);
            sb.append("\",");
            sb.append("\"ItemType\":");
            sb.append("\"");
            sb.append(ItemType);
            sb.append("\",");
            sb.append("\"qty\":");
            sb.append(qty);
            sb.append("}");
            return sb.toString();
        }

        private String escapeDoubleQuote(String str) {
            if(str == null) return "";
            return str.replaceAll("\"", "\\\"");
        }
    }

    public JSONGDItem JSONifyItem(GDItem item) {
        JSONGDItem jsItem = new JSONGDItem();
        jsItem.name = item.getCompleteNiceName();
        jsItem.baseName = item.getBaseNiceName();
        jsItem.prefix = item.getPrefixName();
        jsItem.suffix = item.getSuffixName();
        jsItem.isPartOfASet = item.isPartOfASet();
        jsItem.itemSetName = item.isPartOfASet() ? item.getItemSetName() : "";
        jsItem.rarity = item.getRarity().getNiceName();
        jsItem.baseItemType = Utils.UpperCaseFirstLetter(item.getGearType().getParent().name());
        jsItem.ItemType = Utils.UpperCaseFirstLetter(item.getGearType().name());
        jsItem.owners = item.getOwners().stream().map(o -> o.getName()).collect(Collectors.toSet());
        return jsItem;
    }

    public String buildJSONItemList(List<JSONGDItem> items) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        List<String> strItems = items.stream().map(JSONGDItem::asJSONString).collect(Collectors.toList());
        sb.append(String.join(",", strItems));
        sb.append("];");
        return sb.toString();
    }

    @Override
    public String outputStuff(Map<GDItem, Long> legendariesByQty) {
        List<JSONGDItem> items = new ArrayList<>();
        legendariesByQty.forEach((item, qty) -> {
            JSONGDItem jsItem = JSONifyItem(item);
            jsItem.qty = qty;
            items.add(jsItem);
        });
        return buildJSONItemList(items);
    }
}
