package knoo.gdie.output;

import knoo.gdie.Main;
import knoo.gdie.misc.Utils;
import knoo.gdie.sack.item.GDItem;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by knoodrake on 29/03/2017.
 */
public class TextOutputator implements Outputator {


    private boolean debug;
    private final static String NL = System.getProperty("line.separator");

    public TextOutputator() {
        this.debug = Main.getConfig().getSectionMainSettings().getDebug();
    }

    @Override
    public String outputStuff(Map<GDItem, Long> legendariesByQty) {
        StringBuilder sb = new StringBuilder();

        final int[] maxItemNameLength = {0};
        legendariesByQty.forEach((i,q) -> {
            if(i.getCompleteNiceName().length() > maxItemNameLength[0])
                maxItemNameLength[0] = i.getCompleteNiceName().length();
        });

        if(Main.getConfig().getSectionItems().getOutputNonDuplicate()) {
            sb.append("_________________________");
            sb.append(NL);
            sb.append("Unique items");
            sb.append(NL);
            sb.append("_________________________");
            sb.append(NL);
            legendariesByQty.forEach((item, num) -> {
                if (!item.isJuggedDuplicate()) {
                    if (debug) {
                        sb.append(item.toString());
                    } else {
                        nicePrintTheItem(sb, item, num, maxItemNameLength[0]);
                    }
                }
            });
        }

        sb.append(NL);
        sb.append("_________________________");
        sb.append(NL);
        sb.append("items in duplicate");
        sb.append(NL);
        sb.append("_________________________");
        sb.append(NL);
        legendariesByQty.forEach((item, num) -> {
            if (item.isJuggedDuplicate()) {
                if (debug) {
                    sb.append(item.toString());
                } else {
                    nicePrintTheItem(sb, item, num, maxItemNameLength[0] );
                }
            }
        });

        if(debug) {
            sb.append("==================");
            sb.append(NL);
            sb.append("Outputed ");
            sb.append(legendariesByQty.size());
            sb.append(" individuals item lines");
            sb.append(NL);
            sb.append("==================");
            sb.append(NL);
        }

        return sb.toString();
    }

    public void nicePrintTheItem(StringBuilder sb, GDItem item, Long num, int maxItemNameLength) {
        sb.append(Utils.padRight(num.toString(), 2));
        sb.append(" ");
        sb.append(Utils.UpperCaseFirstLetter(item.getGearType().getParent().name().substring(0, 4)));
        sb.append(" ");
        sb.append(Utils.UpperCaseFirstLetter(Utils.padRight(item.getGearType().name(), 4)));
        sb.append(" ");
        sb.append(Utils.UpperCaseFirstLetter(item.getRarity().name().substring(0, 4)));
        sb.append(" ");
        if(item.isPartOfASet()){
            sb.append("Set ");
        } else {
            sb.append("    ");
        }
        sb.append(Utils.padRight(item.getCompleteNiceName(), maxItemNameLength));
        sb.append(" Owned by:");
        sb.append(String.join(", ", item.getOwners().stream().map(owner -> owner.getName()).collect(Collectors.toList())));
        sb.append(NL);
    }
}
