package knoo.gdie.output;

import knoo.gdie.Main;
import knoo.gdie.sack.item.GDItem;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by knoodrake on 04/04/2017.
 */
public class JSHTMLOutputator extends JSONOutputator {

    private final static String TEMPLATE_FILE_NAME = "/outputTemplates/htmlTemplate.html";
    private final static String DATA_PLACEHOLDER =  "##JSONDATA##";
    private final static String QUERY_URL_PLACEHOLDER =  "##QUERYURLPLACEHOLDER##";
    private final static String QUERY_URL_TEMPLATE =  "##QUERYURLTEMPLATE##";
    private final static String DO_LINK =  "##DOLINK##";

    @Override
    public String outputStuff(Map<GDItem, Long> legendariesByQty) {
        String json = super.outputStuff(legendariesByQty);

        InputStream templateStream = ClassLoader.class.getResourceAsStream(TEMPLATE_FILE_NAME);
        String templateStr = new BufferedReader(new InputStreamReader(templateStream)).lines()
                .parallel().collect(Collectors.joining("\n"));

        if( Main.getConfig().getSectionHtml().getLinkItems() ) {
            templateStr = templateStr
                    .replaceAll(DO_LINK, "true")
                    .replaceAll(QUERY_URL_PLACEHOLDER, Main.getConfig().getSectionMainSettings().getLinkItemsItemPlaceholder())
                    .replaceAll(QUERY_URL_TEMPLATE, Main.getConfig().getSectionMainSettings().getLinkItemsQuery());
        }

        return templateStr.replaceAll(DATA_PLACEHOLDER, json);
    }

}
