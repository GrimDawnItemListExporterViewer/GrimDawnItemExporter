package knoo.gdie.output;

import knoo.gdie.Main;
import knoo.gdie.sack.item.GDItem;

import java.util.Map;

/**
 * Created by knoodrake on 29/03/2017.
 */
public class CSVOutputator implements Outputator {

    protected final static String NL = System.getProperty("line.separator");

    private final Boolean skipHeader;
    private String separator = ",";

    public CSVOutputator() {
        this.skipHeader = Main.getConfig().getSectionCsv().getSkipHeaderRow();
        this.separator =  Main.getConfig().getSectionCsv().getSeparator();
    }

    @Override
    public String outputStuff(Map<GDItem, Long> legendariesByQty) {
        StringBuilder sb = new StringBuilder();
        if(!(Boolean.TRUE.equals(skipHeader))) {
            sb.append("Qty");
            sb.append(separator);
            sb.append("Item");
            sb.append(separator);
            sb.append("rarity");
            sb.append(separator);
            sb.append("isPartOfSet");
            sb.append(separator);
            sb.append("type");
            sb.append(separator);
            sb.append("subType");
            sb.append(NL);
        }
        legendariesByQty.forEach((item, qty) -> {
            if(item.isJuggedDuplicate() || Main.getConfig().getSectionItems().getOutputNonDuplicate()) {
                sb.append(qty);
                sb.append(separator);
                sb.append(item.getCompleteNiceName());
                sb.append(separator);
                sb.append(item.getRarity().name());
                sb.append(separator);
                sb.append(item.isPartOfASet());
                sb.append(separator);
                sb.append(item.getGearType().getParent());
                sb.append(separator);
                sb.append(item.getGearType());
                sb.append(NL);
            }
        });
        return sb.toString();
    }
}
