package knoo.gdie;

import knoo.gdie.file.FileReader.GDBinaryFileReader;
import knoo.gdie.file.FileReader.GDFormulaFileReader;
import knoo.gdie.file.decryptor.GDCDecoder;
import knoo.gdie.file.decryptor.GSTDecoder;
import knoo.gdie.file.decryptor.GSTFormulasDecoder;
import knoo.gdie.log.GDIELogger;
import knoo.gdie.sack.item.GDItem;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by knoodrake on 03/04/2017.
 */
public class GameFilesReader {

    final List<GDItem> items = new ArrayList<>();

    private boolean haveRead = false;

    private GSTFormulasDecoder formulasDecoder;
    private GSTDecoder gstDecoder;
    private GDCDecoder gdcDecoder;

    public GameFilesReader() {
        GDBinaryFileReader binaryFileReader = new GDBinaryFileReader();
        GDFormulaFileReader formulaFileReader = new GDFormulaFileReader();

        formulasDecoder = new GSTFormulasDecoder(formulaFileReader);
        gstDecoder = new GSTDecoder(binaryFileReader);
        gdcDecoder = new GDCDecoder(binaryFileReader);

    }

    public List<GDItem> read() throws IOException {
        haveRead = false;
        clear();

        // Read Formulas file
        GDIELogger.get().log("Reading formulas file");
        formulasDecoder.decode(Main.getConfig().getSectionMainSettings().getSavePath() + "/formulas.gst");
        formulasDecoder.getItemContainers().stream().forEach(container -> items.addAll(container.getItems()));

        // Read Shared Stash File
        GDIELogger.get().log("Reading shared stash file");
        gstDecoder.decode(Main.getConfig().getSectionMainSettings().getSavePath() + "/transfer.gst");
        gstDecoder.getItemContainers().stream().forEach(container -> items.addAll(container.getItems()));

        // Read character files
        File[] files = new File(Main.getConfig().getSectionMainSettings().getSavePath() + "/main/").listFiles();
        for (File file : files) {
            if (file.isDirectory() && file.getName().startsWith("_")) {
                GDIELogger.get().log("Reading: " +  file.getName().replace("_", ""));
                gdcDecoder.decode(file.getAbsolutePath() + "/player.gdc");
                gdcDecoder.getItemContainers().stream().forEach(container -> items.addAll(container.getItems()));
                gdcDecoder.clear(false);
            }
        }

        haveRead = true;
        return items;
    }

    public void clear() {
        items.clear();
        gdcDecoder.clear(true);
        gstDecoder.clear(true);
        formulasDecoder.clear(true);
        haveRead = false;
    }

    public List<GDItem> getItems() {
        return items;
    }

    public boolean haveRead() {
        return haveRead;
    }
}
