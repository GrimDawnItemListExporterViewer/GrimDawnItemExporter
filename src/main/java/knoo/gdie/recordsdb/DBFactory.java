package knoo.gdie.recordsdb;

import knoo.gdie.log.GDIELogger;

/**
 * Created by knoodrake on 09/04/2017.
 */
public class DBFactory {

    private static RecordsDB __instance = null;
    private static Class<? extends RecordsDB> defaultDB = JGrimARZDB.class;

    public static RecordsDB getDB() {
        if(__instance == null) {
            try {
                __instance = defaultDB.newInstance();
            } catch (InstantiationException e) {
                GDIELogger.get().log(e);
            } catch (IllegalAccessException e) {
                GDIELogger.get().log(e);
            }
        }
        return __instance;
    }
}
