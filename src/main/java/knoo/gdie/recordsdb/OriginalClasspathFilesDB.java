package knoo.gdie.recordsdb;

import knoo.gdie.log.GDIELogger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Created by knoodrake on 09/04/2017.
 */
class OriginalClasspathFilesDB implements RecordsDB {

    @Override
    public void init(String gdDBPath) throws IOException {
    }

    @Override
    public Map<String, String> get(String recordName) {
        Map<String,String> itemProperties = new WeakHashMap<>();
        try{
            InputStream is = ClassLoader.class.getResourceAsStream("/db/" + recordName);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            br.lines().forEach((line) -> {
                String[] p = line.split(",");
                itemProperties.put(p[0], (p.length == 1 ? null : p[1]));
            });
            br.close();
        } catch (IOException e) {
            GDIELogger.get().log(e);
        }
        return itemProperties ;
    }

    @Override
    public boolean isARecordPath(String value) {
        return value.startsWith("records/");
    }

    @Override
    public String get(String recordName, String key) {
        Map<String, String> dbrFile = get(recordName);
        if(dbrFile == null) return null;
        return dbrFile.get(key);
    }
}
