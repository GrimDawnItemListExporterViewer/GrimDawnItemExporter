package knoo.gdie.recordsdb;

import java.io.IOException;
import java.util.Map;

/**
 * Created by knoodrake on 08/04/2017.
 */
public interface RecordsDB {

    void init(final String gdDBPath ) throws IOException;

    Map<String, String> get(final String recordName);

    boolean isARecordPath(final String value);

    String get(final String recordName, final String key);
}
