package knoo.gdie.recordsdb;

//import knoo.gdie.log.GDIELogger;
//import org.mapdb.DB;
//import org.mapdb.DBMaker;
//
//import java.io.File;
//import java.io.IOException;
//import java.net.URISyntaxException;
//import java.net.URL;
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.concurrent.ConcurrentMap;

/**
 * Created by knoodrake on 07/04/2017.
 */
class RecordsMemoryDB /** extends AbstractFromClassPathDB **/ {
/**
    private ConcurrentMap<String, Map<String, String>> records;

    @Override
    public void init(String gdDBPath) throws IOException {
        GDIELogger.get().log("Initializing items DB...");
        URL dbFileUrl = getClass().getResource("/recordsDB.db");
        try {
            File dbFile = new File(dbFileUrl.toURI());
            DB db = DBMaker.fileDB(dbFile).fileMmapEnable().make();
            records = (ConcurrentMap<String, Map<String, String>>) db.hashMap("map").open();
            db.close();
        } catch (URISyntaxException e) {
            GDIELogger.get().log(e);
        }
//        getResourceFilesByName(gdDBPath).forEach((file, content) -> {
//            records.put(file, handleContent(content));
//        });
        GDIELogger.get().log("DB Initialization done.");
    }

    private Map<String, String> handleContent(String content) {
        Map<String, String> contentMap = new HashMap<>();
        Arrays.asList(content.split(System.getProperty("line.separator"))).stream().forEach(line -> {
            if(line.trim().isEmpty()) return;
            int commaPos = line.indexOf(",");
            contentMap.put(
                line.substring(0, commaPos),
                line.substring(commaPos, line.length())
            );

        });
        return contentMap;
    }

    @Override
    public Map<String, String> get(String recordName) {
        return records.get(recordName);
    }

    @Override
    public boolean isARecordPath(String value) {
        if(value == null)return false;
        return value.startsWith("records/");
    }

    @Override
    public String get(String recordName, String key) {
        Map<String, String> dbrFile = records.get(recordName);
        if(dbrFile == null) return null;
        return dbrFile.get(key);
    }
    **/
}
