package knoo.gdie.recordsdb;

import knoo.gdie.ARZDatabase;
import knoo.gdie.ARZDatabaseReader;
import knoo.gdie.ARZRecord;
import knoo.gdie.misc.GameFileFinder;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

/**
 * Created by knoodrake on 26/04/2017.
 */
public class JGrimARZDB implements RecordsDB {

    private ARZDatabaseReader arzDatabaseReader;

    public JGrimARZDB() {
        arzDatabaseReader = new ARZDatabaseReader();
        arzDatabaseReader.setRecordNameFilter(recordName -> {
            if(!recordName.startsWith("records/items/")) return false;
            return !Arrays.asList(
                "oneshot_scroll.tpl",
                "crafting/consumables/",
                "items/materia",
                "crafting/materials/"
            ).stream().anyMatch(s -> recordName.contains(s));
        });
        arzDatabaseReader.setKeepOnlyValues(Arrays.asList(
                "itemClassification",
                "artifactClassification",
                "itemSetName",
                "setName",
                "lootRandomizerName",
                "itemNameTag",
                "artifactName",
                "Class",
                "FileDescription",
                "itemStyleTag",
                "ItemArtifactFormula",

                // TODO Test
                "lootRandomizerJitter",
                "lootRandomizerCost",
                "levelRequirement"
        ));
        arzDatabaseReader.setRecordMustHaveAtLeastOneOf(Arrays.asList(
                "itemClassification",
                "artifactClassification",
                "Class",
                "setName"
        ));
    }

    @Override
    public void init(String gdDBPath) throws IOException {
        arzDatabaseReader.init(GameFileFinder.findGameDBFile());
    }

    @Override
    public Map<String, String> get(String recordName) {
        ARZRecord arzRecord = ARZDatabase.get().getRecords().get(recordName);
        return arzRecord == null ? null : arzRecord.getRecordFileData();
    }

    @Override
    public boolean isARecordPath(String value) {
        return value.startsWith("records/");
    }

    @Override
    public String get(String recordName, String key) {
        Map<String, String> stringStringMap = get(recordName);
        if(stringStringMap == null) return null;
        return stringStringMap.get(key);
    }
}
