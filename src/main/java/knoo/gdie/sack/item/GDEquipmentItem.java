package knoo.gdie.sack.item;

import knoo.gdie.file.FileReader.GDBinaryFileReader;
import knoo.gdie.sack.character.GDCharacter;
import knoo.gdie.sack.item.container.ItemContainer;
import knoo.gdie.sack.item.container.ItemContainerType;

import java.io.IOException;

/**
 * Created by knoodrake on 29/03/2017.
 */
public class GDEquipmentItem extends GDItem {

    protected boolean isAttached = false;

    public GDEquipmentItem(GDBinaryFileReader reader, ItemContainer container, GDCharacter owner) {
        super(reader, container, owner);
        currentlyEquippedByInitialOwner = true;
    }



    @Override
    public boolean read() throws IOException {
        boolean b = super.read();
        isAttached = reader.rByte() > 0;
        return b;
    }
}
