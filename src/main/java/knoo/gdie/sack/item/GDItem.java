package knoo.gdie.sack.item;

import knoo.gdie.file.FileReader.GDBinaryFileReader;
import knoo.gdie.log.GDIELogger;
import knoo.gdie.recordsdb.DBFactory;
import knoo.gdie.recordsdb.RecordsDB;
import knoo.gdie.sack.character.GDCharacter;
import knoo.gdie.sack.item.container.ItemContainer;
import knoo.gdie.sack.item.container.ItemContainerType;
import knoo.gdie.sack.item.gear.GearType;

import java.io.*;
import java.util.*;

/**
 * Created by knoodrake on 27/03/2017.
 */
public class GDItem implements Comparable<GDItem>{

    protected final static String NL = System.getProperty("line.separator");
    private static GDItemComparator comparator = new GDItemComparator();

    protected GDBinaryFileReader reader;
    protected ItemContainer container;
    protected String itemID;
    protected String prefixID;
    protected String suffixID;
    protected String modifierID;
    protected String transmuteID;
    protected int itemSeed;
    protected String componentID;
    protected String completionBonusID;
    protected int componentSeed;
    protected String augmentID;
    protected int unknown;
    protected int augmentSeed;
    protected int var1;
    protected int stackCount;
    protected int x;
    protected int y;

    protected ItemRarity rarity = ItemRarity.UNKNOWN;
    protected String completeNiceName = "unknown";
    protected String prefixName = "";
    protected String suffixName = "";
    protected String baseNiceName = "";
    protected boolean isOfInterest = false;
    protected Map<String,String> dbrProperties = null;
    protected boolean juggedDuplicate = false;
    protected boolean isPartOfASet = false;
    protected GearType gearType = GearType.GEAR;
    protected String itemNameTag;
    protected GDCharacter initialOwner = null;

    protected String itemSetName = null;

    protected boolean currentlyEquippedByInitialOwner = false;
    protected List<GDCharacter> owners = new ArrayList<>();
    protected boolean isAnArtifact = false;

    protected boolean isEmpowered = false;


    // TODO just a test for memleaks
    public void commitSuicide() {
        this.reader = null;
        this.container = null;
        this.initialOwner = null;
        this.owners.clear();
        this.owners = null;
        this.dbrProperties.clear();
        this.dbrProperties = null;
    }

    public GDCharacter getInitialOwner() {
        return initialOwner;
    }

    public boolean isCurrentlyEquippedByInitialOwner() {
        return currentlyEquippedByInitialOwner;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(x);
        sb.append(";");
        sb.append(y);
        sb.append("] ");
        sb.append(stackCount);
        sb.append("x ");
        sb.append(completeNiceName);
        sb.append(", itemID: ");
        sb.append(itemID);
        sb.append(", GearType: ");
        sb.append(gearType.getParent());
        sb.append(":");
        sb.append(gearType);
        sb.append(", prefixID: ");
        sb.append(prefixID);
        sb.append(", suffixID: ");
        sb.append(suffixID);
        sb.append(", modifierID: ");
        sb.append(modifierID);
        sb.append(", transmuteID: ");
        sb.append(transmuteID);
        sb.append(", componentID: ");
        sb.append(componentID);
        sb.append(NL);
        return sb.toString();
    }

    public String getCompleteNiceName() {
        return completeNiceName;
    }

    public void analyzeDBRItemProperties() {
        if(dbrProperties == null) {
            isOfInterest = false;
            return;
        }

        this.isEmpowered = analyzeEmpowered();
        analyzeItemSet();
        analyzeItemRarity();

        // TODO
        //if (TODO_analyzeExaltedThreads()) return;

        if(ItemRarity.RARE.getRarityLevel() > getRarity().getRarityLevel()) {
            isOfInterest = false;
            return;
        }

        // TODO most of these items types are now altready filtered by RecordsDB: JGrimARZDB...
        // ( so it's redundant.. but what if we don't use the JGrimARZDB ? )

        if(itemID.startsWith("records/items/gear")) {
            isOfInterest = true;
            analyzeGearType();
        }
        if(itemID.endsWith("oneshot_scroll.tpl")) {
            completeNiceName = "scroll";
            isOfInterest = false;
            return;
        }
        if(itemID.contains("crafting/consumables/")) {
            completeNiceName = "consumable";
            isOfInterest = false;
            return;
        }
        if(itemID.contains("crafting/blueprints/")) {
            completeNiceName = "blueprint";
            analyzeBP();
            return;
        }
        if(itemID.contains("items/materia")) {
            isOfInterest = false;
            return;
        }
        if(itemID.contains("crafting/materials/")) {
            // Tainted Brain matters, Blood of Ch'Thon, ...
            isOfInterest = false;
            return;
        }
        String fileDesc = dbrProperties.get("FileDescription");
        itemNameTag = dbrProperties.get("itemNameTag");
        completeNiceName = ItemNames.getName(itemNameTag);
        if(completeNiceName == null) {
            completeNiceName = fileDesc;
            if(fileDesc == null) {
                completeNiceName = "unknown";
            }
        }
        baseNiceName = completeNiceName;
        if(isEmpowered()) {
            completeNiceName = "Empowered " + completeNiceName;
        }
        analyzeAffixe(true, prefixID);
        analyzeAffixe(false, suffixID);
    }

    public boolean analyzeEmpowered(Map<String, String> propMap) {
        return "Empowered".equals(ItemNames.getName(propMap.get("itemStyleTag")));
    }

    public boolean analyzeEmpowered() {
        return analyzeEmpowered(dbrProperties);
    }

    // TODO WIP todo: add filter for them
    // TODO check level requierment
    // TODO FIXME: Exalted Threads: veeeery ugly tmp code
    public boolean TODO_analyzeExaltedThreads() {
        if("records/items/gearfeet/a09_feet02.dbr".equals(itemID)) {
            String itemName = ItemNames.getName(dbrProperties.get("itemNameTag"));
            String tagStyle = dbrProperties.get("itemStyleTag");
            if(tagStyle != null && !tagStyle.isEmpty()) {
                String itemStyleName = ItemNames.getName(tagStyle);
                if("Exalted".equals(itemStyleName)) {

                    analyzeAffixe(true, prefixID);
                    if(prefixName.toLowerCase().contains("renegade")) {
                        Map<String, String> stringStringMap = DBFactory.getDB().get(prefixID);
                        System.err.println("");
                    }


                    if(!suffixID.isEmpty() && !prefixID.isEmpty()) {
//                        int prefLevelRequirement = 0; try { prefLevelRequirement = Integer.valueOf(DBFactory.getDB().get(prefixID, "levelRequirement")); } catch (Exception e) {}
//                        int suffLevelRequirement = 0; try { suffLevelRequirement = Integer.valueOf(DBFactory.getDB().get(suffixID, "levelRequirement")); } catch (Exception e) {}
                        ItemRarity prefixRarity = ItemRarity.valueOf(DBFactory.getDB().get(prefixID, "itemClassification").toUpperCase());
                        ItemRarity suffixRarity = ItemRarity.valueOf(DBFactory.getDB().get(suffixID, "itemClassification").toUpperCase());
//                        if("of kings".equalsIgnoreCase(suffixName)) {
//                        if(prefLevelRequirement >= 75 || suffLevelRequirement >= 75) {
                        if(ItemRarity.RARE.equals(prefixRarity) || ItemRarity.RARE.equals(suffixRarity)) {
                            analyzeAffixe(true, prefixID);
                            analyzeAffixe(false, suffixID);
                            baseNiceName = itemStyleName + " " + itemName;
                            completeNiceName = prefixName + " " + baseNiceName + " " + suffixName;
                            // TODO see above
                            isOfInterest = true;
                            gearType = GearType.FEET;
                            rarity = ItemRarity.W_RARE_AFFIX;
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private void analyzeBP() {
        if("ItemArtifactFormula".equals(dbrProperties.get("Class"))) {
            String artifactName = dbrProperties.get("artifactName");
            if(artifactName != null && artifactName.startsWith("records/")) {
                Map<String, String> artifacteDBRFile = DBFactory.getDB().get(artifactName);
                if (artifacteDBRFile != null) {
                    this.gearType = GearType.LEARNED_BLUEPRINTS;
                    String bpNameTag = artifacteDBRFile.get("itemNameTag");
                    if (bpNameTag != null) {
                        analyzeItemSet(artifacteDBRFile);
                        boolean empowered = analyzeEmpowered(artifacteDBRFile);
                        String name = ItemNames.getName(bpNameTag);
                        completeNiceName = "Can craft: " + (empowered ? "Empowered " : "") + name;
                        baseNiceName = name;
                        isOfInterest = true;
                        return;
                    }
                }
            }
        }
    }

    private void analyzeAffixe(boolean isPrefix, String affixeID) {
        String affixeTag = DBFactory.getDB().get(affixeID, "lootRandomizerName");
        if(affixeTag != null) {
            final String affixe = ItemNames.getName(affixeTag);
            if(affixe != null && !affixe.isEmpty()) {
                if(isPrefix) {
                    completeNiceName = affixe + " "+ completeNiceName;
                    prefixName = affixe;
                }
                else {
                    completeNiceName = completeNiceName + " " + affixe;
                    suffixName = affixe;
                }
                return;
            }
        }
    }

    private void analyzeGearType() {
        final String[] strEnd = itemID.substring("records/items/gear".length()).split("/");
        final String first = strEnd[0];
        final String second = strEnd[1];
        switch (first) {
            case "accessories":
                switch (second) {
                    case "medals": gearType = GearType.MEDALS; break;
                    case "necklaces": gearType = GearType.NECKLACES; break;
                    case "rings": gearType = GearType.RINGS; break;
                    case "waist": gearType = GearType.BELT; break;
                }
                break;
            case "feet": gearType = GearType.FEET; break;
            case "hands": gearType = GearType.HANDS; break;
            case "head": gearType = GearType.HEAD; break;
            case "legs": gearType = GearType.LEGS; break;
            case "relic": gearType = GearType.RELIC; break;
            case "shoulders": gearType = GearType.SHOULDER; break;
            case "torso": gearType = GearType.TORSO; break;
            case "weapons": gearType = GearType.FEET;
                switch (second) {
                    case "axe1h": gearType = GearType.MELEE1H; break;
                    case "blunt1h": gearType = GearType.MELEE1H; break;
                    case "caster": gearType = GearType.MELEE1H; break;
                    case "guns1h": gearType = GearType.RANGED1H; break;
                    case "focus": gearType = GearType.OFFHAND; break;
                    case "guns2h": gearType = GearType.RANGED2H; break;
                    case "melee2h": gearType = GearType.MELEE2H; break;
                    case "shields": gearType = GearType.SHIELD; break;
                    case "swords1h": gearType = GearType.MELEE1H; break;
                }
                break;
            default:
                gearType = GearType.UNKNOWN;
                isOfInterest = false;
                break;
        }
    }

    private void analyzeItemSet() {
        analyzeItemSet(dbrProperties);
    }

    private void analyzeItemSet(Map<String,String> propMap) {
        String itemSetName = propMap.get("itemSetName");
        if(itemSetName == null) return;
        if(itemSetName.isEmpty()) return;
        Map<String, String> stringStringMap = DBFactory.getDB().get(itemSetName);
        String setName = stringStringMap.get("setName");
        this.itemSetName = ItemNames.getName(setName);
        isPartOfASet = true;
    }

    public void analyzeItemRarity() {
        String artifactClassification = dbrProperties.get("artifactClassification");
        if(artifactClassification == null) {
            String itemClassification = dbrProperties.get("itemClassification");
            if (itemClassification == null) return;
            switch (itemClassification) {
                case "Epic":
                    rarity = ItemRarity.EPIC;
                    break;
                case "Rare":
                    rarity = ItemRarity.RARE;
                    break;
                case "Legendary":
                    rarity = ItemRarity.LEGENDARY;
                    break;
                case "Common":
                    rarity = ItemRarity.COMMON;
                    break;
                default:
                    break;
            }
        } else {
            isAnArtifact = true;
        }
    }

    public GDItem(GDBinaryFileReader reader, ItemContainer container, GDCharacter owner) {
        this.reader = reader;
        this.container = container;
        this.initialOwner = owner;
    }

    public boolean read() throws IOException {
        String s = null;
        int i = 0;
        s = reader.rStr(); // 1: ID
        this.setItemID(s);
        s = reader.rStr(); // 2 Prefix
        this.setPrefixID(s);
        s = reader.rStr(); // 3 suffix
        this.setSuffixID(s);
        s = reader.rStr(); // 4 modifier
        this.setModifierID(s);
        s = reader.rStr(); // 5 transmute
        this.setTransmuteID(s);
        i = reader.rInt(); // 6 seed
        this.setItemSeed(i);
        s = reader.rStr(); // 7 componend
        this.setComponentID(s);
        s = reader.rStr(); // 8 compl bonus
        this.setCompletionBonusID(s);
        i = reader.rInt(); // 9 compl seed
        this.setComponentSeed(i);
        s = reader.rStr(); // 10 augm ID
        this.setAugmentID(s);
        i = reader.rInt(); // 11 unknown
        this.setUnknown(i);
        i = reader.rInt(); // 12 augm seed
        this.setAugmentSeed(i);
        i = reader.rInt(); // 13 var1
        this.setVar1(i);
        i = reader.rInt(); // 14 stackCount
        this.setStackCount(i);


        if(container.getItemContainerType() == ItemContainerType.CHAR_INVENTORY) {
            this.x = reader.rInt();
            this.y = reader.rInt();
        } else if (container.getItemContainerType() == ItemContainerType.CHAR_EQUIPMENT) {
            this.x = 0;
            this.y = 0;
        } else {
            this.x = (int) reader.rFloat();
            this.y = (int) reader.rFloat();
        }

        if(itemID.isEmpty()) {
            // NO ITEM ( Ex: 2nd Weapon when wearing 2H weapon, or unequiped place on char )
            completeNiceName = "! empty slot";
            return false;
        }
        if(!itemID.startsWith("records/items/")) {
            completeNiceName = "! unsupported item";
            return false;
        }

        dbrProperties = DBFactory.getDB().get(itemID);
        analyzeDBRItemProperties();
        return true;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof GDItem)) {
            return false;
        }

        GDItem that = (GDItem) other;
        return this.completeNiceName.equals(that.getCompleteNiceName())
                && this.completeNiceName.equals(that.getCompleteNiceName());
    }

    @Override
    public int hashCode() {
        int hashCode = 1;

        hashCode = hashCode * 37 + this.completeNiceName.hashCode();
        hashCode = hashCode * 37 + this.completeNiceName.hashCode();

        return hashCode;
    }

    public void setGearType(GearType gearType) {
        this.gearType = gearType;
    }

    public GearType getGearType() {
        return gearType;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public String getItemID() {
        return itemID;
    }

    public void setPrefixID(String prefixID) {
        this.prefixID = prefixID;
    }

    public String getPrefixID() {
        return prefixID;
    }

    public void setSuffixID(String suffixID) {
        this.suffixID = suffixID;
    }

    public String getSuffixID() {
        return suffixID;
    }

    public void setModifierID(String modifierID) {
        this.modifierID = modifierID;
    }

    public String getModifierID() {
        return modifierID;
    }

    public void setTransmuteID(String transmuteID) {
        this.transmuteID = transmuteID;
    }

    public String getTransmuteID() {
        return transmuteID;
    }

    public void setItemSeed(int itemSeed) {
        this.itemSeed = itemSeed;
    }

    public int getItemSeed() {
        return itemSeed;
    }

    public void setComponentID(String componentID) {
        this.componentID = componentID;
    }

    public String getComponentID() {
        return componentID;
    }

    public void setCompletionBonusID(String completionBonusID) {
        this.completionBonusID = completionBonusID;
    }

    public String getCompletionBonusID() {
        return completionBonusID;
    }

    public boolean isEmpowered() {
        return isEmpowered;
    }

    public void setComponentSeed(int componentSeed) {
        this.componentSeed = componentSeed;
    }

    public int getComponentSeed() {
        return componentSeed;
    }

    public void setAugmentID(String augmentID) {
        this.augmentID = augmentID;
    }

    public String getAugmentID() {
        return augmentID;
    }

    public void setUnknown(int unknown) {
        this.unknown = unknown;
    }

    public int getUnknown() {
        return unknown;
    }

    public void setAugmentSeed(int augmentSeed) {
        this.augmentSeed = augmentSeed;
    }

    public int getAugmentSeed() {
        return augmentSeed;
    }

    public void setVar1(int var1) {
        this.var1 = var1;
    }

    public int getVar1() {
        return var1;
    }

    public void setStackCount(int stackCount) {
        this.stackCount = stackCount;
    }

    public int getStackCount() {
        return stackCount;
    }

    public boolean isOfInterest() {
        return isOfInterest;
    }

    public ItemRarity getRarity() {
        return rarity;
    }

    public String getItemSetName() {
        return itemSetName;
    }

    public boolean isJuggedDuplicate() {
        return juggedDuplicate;
    }

    public void setJuggedDuplicate(boolean juggedDuplicate) {
        this.juggedDuplicate = juggedDuplicate;
    }

    public boolean isPartOfASet() {
        return isPartOfASet;
    }

    public void setPartOfASet(boolean isPartOfASet) {
        this.isPartOfASet = isPartOfASet;
    }

    @Override
    public int compareTo(GDItem o) {
        return comparator.compare(this, o);
    }

    public void setOwners(List<GDCharacter> owners) {
        this.owners = owners;
    }

    public List<GDCharacter> getOwners() {
        return owners;
    }

    public String getPrefixName() {
        return prefixName;
    }

    public String getSuffixName() {
        return suffixName;
    }

    public String getBaseNiceName() {
        return baseNiceName;
    }

    public boolean isAnArtifact() {
        return isAnArtifact;
    }

    public ItemContainer getContainer() {
        return container;
    }
}
