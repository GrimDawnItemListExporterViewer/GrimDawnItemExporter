package knoo.gdie.sack.item;

import knoo.gdie.log.GDIELogger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by knoodrake on 31/03/2017.
 */
public class ItemNames {

    private static Map<String, String> namesByTags = new HashMap<>();
    private static boolean initializated = false;

    public static void init() {
        if(initializated) return;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(ClassLoader.class.getResourceAsStream("/db/text/tags_items.txt")));
            String line;
            while ((line = reader.readLine()) != null) {
                handleLine(line);
            }
            initializated = true;
            reader.close();
        } catch (IOException e) {
            GDIELogger.get().log(e);
        }
    }

    private static void handleLine(String line) {
       if(line.startsWith("#")) return;
       if(line.trim().isEmpty()) return;
        String[] split = line.split("=");
        if(split.length == 1)
            namesByTags.put(split[0], "");
        else
            namesByTags.put(split[0], split[1]);
    }

    public static String getName(final String itemTag) {
        return namesByTags.get(itemTag);
    }
}
