package knoo.gdie.sack.item;

/**
 * Created by knoodrake on 29/03/2017.
 */
public enum ItemRarity {
    UNKNOWN(-1, "unknown"),
    COMMON(0, "Common"),
    MAGICAL(1, "Magical"),
    W_RARE_AFFIX(2,"Made Rare by Rare Affix"),
    RARE(3,"MIs"),
    EPIC(4,"Epic"),
    LEGENDARY(5,"Legendary");

    private int rarityLevel;
    private final String niceName;

    ItemRarity(int rarity, final String niceName) {
        this.rarityLevel = rarity;
        this.niceName = niceName;
    }

    public int getRarityLevel() {
        return rarityLevel;
    }

    public String getNiceName() {
        return niceName;
    }
}
