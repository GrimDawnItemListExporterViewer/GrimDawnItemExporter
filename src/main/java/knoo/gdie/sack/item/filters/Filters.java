package knoo.gdie.sack.item.filters;

import knoo.gdie.Main;
import knoo.gdie.log.GDIELogger;
import knoo.gdie.misc.Utils;
import knoo.gdie.output.JSONOutputator;
import knoo.gdie.sack.item.GDItem;
import knoo.gdie.sack.item.ItemRarity;
import knoo.gdie.sack.item.container.ItemContainerType;
import knoo.gdie.sack.item.gear.GearType;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by knoodrake on 10/04/2017.
 */
public class Filters {

    public static Predicate<GDItem> skippedEquippedItems() {
        if(Main.getConfig().getSectionItems().getSkipEquippedItems())
            return item -> !item.isCurrentlyEquippedByInitialOwner();
        return item -> true;
    }

    public static Predicate<GDItem> noArtifacts() {
        return item -> !item.isAnArtifact();
    }

    public static Predicate<GDItem> onlyThoseOfInterest() {
        return item -> item.isOfInterest();
    }

    public static Predicate<GDItem> skippedSharedStash() {
        return item -> !(Main.getConfig().getSectionItems().getSkipSharedStash()
                && ItemContainerType.SHARED_STASH.equals(item.getContainer().getItemContainerType()));
    }

    public static Predicate<GDItem> matchesSkipItemsSearch() {
        return item -> {
            String skipItemsSearch = Main.getConfig().getSectionItems().getSkipItemsSearch();
            return skipItemsSearch.isEmpty() ? true :
                    !item.getCompleteNiceName().toLowerCase().matches(".*" +
                            skipItemsSearch
                                    .replaceAll("[^*]+", "\\\\Q$0\\\\E").replaceAll("\\*+", ".*") + ".*");
        };
    }

    public static Predicate<GDItem> skippedItems() {
        List<String> skippedItems = Utils.csvStrToLCaseStringList(Main.getConfig().getSectionItems().getSkipItems());
        return item -> !skippedItems.contains(item.getCompleteNiceName().toLowerCase());
    }

    public static Predicate<GDItem> ownedBySkippedCharacters() {
        List<String> skippedCharacters = Utils.csvStrToLCaseStringList(Main.getConfig().getSectionItems().getSkipCharacter());
        return item -> !skippedCharacters.contains(item.getInitialOwner().getName().toLowerCase());
    }

    public static Predicate<GDItem> byRarity() {
        List<ItemRarity> raritiesToOutput = new ArrayList<>();
        if(Main.getConfig().getSectionItems().getOutputLegendaries()) raritiesToOutput.add(ItemRarity.LEGENDARY);
        if(Main.getConfig().getSectionItems().getOutputEpics()) raritiesToOutput.add(ItemRarity.EPIC);
        if(Main.getConfig().getSectionItems().getOutputRares()) {
            raritiesToOutput.add(ItemRarity.RARE);
            raritiesToOutput.add(ItemRarity.W_RARE_AFFIX);
        }
        if(raritiesToOutput.isEmpty()) {
            GDIELogger.get().logErr("All items rarity filtered, nothing to output");
        }
        return item -> raritiesToOutput.contains(item.getRarity());
    }


    public static Predicate<GDItem> skippedNotSetLegendaries() {
        return item -> {
            if(!Main.getConfig().getSectionItems().getSkipLegendaryNoSet()) return true;
            if(!ItemRarity.LEGENDARY.equals(item.getRarity())) return true;
            return item.isPartOfASet();
        };
    }

    public static Predicate<GDItem> skippedNotSetEpics() {
        return item -> {
            if(!Main.getConfig().getSectionItems().getSkipEpicNoSet()) return true;
            if(!ItemRarity.EPIC.equals(item.getRarity())) return true;
            return item.isPartOfASet();
        };
    }

    public static Predicate<GDItem> skippedSharedStashTab() {
        List<Integer> skippedSharedStashTabs = Utils.csvStrToIntegerList(Main.getConfig().getSectionItems().getSkipSharedStashTab());
        return item -> !(
                ItemContainerType.SHARED_STASH.equals(item.getContainer().getItemContainerType()) &&
                skippedSharedStashTabs.contains(item.getContainer().getPosition()+1));
    }

    public static Predicate<GDItem> skippedCharacterInventoryTab() {
        List<Integer> skippedCharInventoryTabs = Utils.csvStrToIntegerList(Main.getConfig().getSectionItems().getSkipCharacterInventoryTab());
        return item -> !(
                ItemContainerType.CHAR_INVENTORY.equals(item.getContainer().getItemContainerType()) &&
                skippedCharInventoryTabs.contains(item.getContainer().getPosition()+1));
    }

    public static Predicate<GDItem> skippedCharacterInventory() {
        return item -> !(Main.getConfig().getSectionItems().getSkipCharacterInventory()
                && ItemContainerType.CHAR_INVENTORY.equals(item.getContainer().getItemContainerType()));
    }

    public static Predicate<GDItem> skippedCharacterStash() {
        return item -> !(Main.getConfig().getSectionItems().getSkipPersonalStash()
                && ItemContainerType.CHAR_STASH.equals(item.getContainer().getItemContainerType()));
    }

    public static Predicate<GDItem> outputBlueprintsAndRecipies() {
        if(Main.getConfig().getSectionItems().getOutputBlueprintsAndRecipies()) return i -> true;
        return item -> !GearType.BLUEPRINTS.equals(item.getGearType().getParent());
    }

    public static Predicate<GDItem> customFilter() {
        if(!Main.getConfig().getSectionCustomFilter().getUseCustomJSFilter()) return i-> true;

        ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
        String customFilter = Main.getConfig().getSectionCustomFilter().getCustomJSFilter();

        JSONOutputator jsonOutputator = new JSONOutputator();

        return i -> {
            Boolean result = Boolean.FALSE;
            try {
                engine.eval("function filter(item) { " + customFilter + "}");
                Invocable invocable = (Invocable) engine;
                JSONOutputator.JSONGDItem javaJsongdItem = jsonOutputator.JSONifyItem(i);
                result = (Boolean) invocable.invokeFunction("filter", javaJsongdItem);
            } catch (ScriptException e) {
                GDIELogger.get().log(e);
            } catch (NoSuchMethodException e) {
                GDIELogger.get().log(e);
            }
            return Boolean.TRUE.equals(result);
        };

    }
}
