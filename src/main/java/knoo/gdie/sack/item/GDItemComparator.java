package knoo.gdie.sack.item;

import java.util.Comparator;

/**
 * Created by knoodrake on 31/03/2017.
 *
 * sort_by rarity > type >  name
 */

public class GDItemComparator implements Comparator<GDItem> {
    public int compare(GDItem o1, GDItem o2) {
        int value1 = new Integer(o1.getRarity().getRarityLevel()).compareTo(new Integer(o2.getRarity().getRarityLevel()));
        if (value1 == 0) {
            int value2 = o1.getGearType().getParent().name().compareTo(o2.getGearType().getParent().name());
            if (value2 == 0) {
                return o1.getCompleteNiceName().compareTo(o2.getCompleteNiceName());
            } else {
                return value2;
            }
        }
        return value1;
    }
}
