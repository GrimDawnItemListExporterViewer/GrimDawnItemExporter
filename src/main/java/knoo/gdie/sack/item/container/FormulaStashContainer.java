package knoo.gdie.sack.item.container;

import knoo.gdie.log.GDIELogger;
import knoo.gdie.sack.item.GDItem;
import knoo.gdie.sack.item.ItemRarity;
import knoo.gdie.sack.item.gear.GDFormulaItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by knoodrake on 08/04/2017.
 */
public class FormulaStashContainer implements ItemContainer {

    protected final static String NL = System.getProperty("line.separator");

    protected List<GDItem> items = new ArrayList<>();

    @Override
    public int getNumOfItems() {
        return items.size();
    }

    public void addRecords(List<String> records) {
        records.stream().forEach(recordStr -> {
            try {
                GDFormulaItem formulaItem = new GDFormulaItem(recordStr, this);
                formulaItem.read();
                if(formulaItem.isOfInterest())
                    items.add(formulaItem);
            } catch (IOException e) {
                GDIELogger.get().log(e);
            }
        });
    }

    @Override
    public ItemContainerType getItemContainerType() {
        return ItemContainerType.FORMULAS;
    }

    @Override
    public List<GDItem> getItems() {
        return items;
    }

    @Override
    public List<GDItem> getItemsAtLeast(ItemRarity rarity) {
        return items.stream().filter(item ->
                        item.getRarity().getRarityLevel() >= rarity.getRarityLevel()
        ).collect(Collectors.toList());
    }
    @Override
    public List<GDItem> getItemsThatAre(ItemRarity rarity) {
        return items.stream().filter(item ->
                        item.getRarity().equals(rarity)
        ).collect(Collectors.toList());
    }

    @Override
    public List<GDItem> getItemsThatAre(List<ItemRarity> rarities) {
        return items.stream().filter(item ->
                        rarities.contains(item.getRarity())
        ).collect(Collectors.toList());
    }

    public String toStringWithItems() {
        StringBuilder sb = new StringBuilder();
        sb.append(toString());
        sb.append(NL);
        for(GDItem item : items) {
            if(item.isOfInterest() && item.getRarity().getRarityLevel() > ItemRarity.EPIC.getRarityLevel()) {
                sb.append(item.toString());
            }
        }
        return sb.toString();
    }

    @Override
    public int getPosition() {
        return 0;
    }

    @Override
    public void clear() {
        items.forEach(i -> i.commitSuicide() );
        items.clear();
    }

}
