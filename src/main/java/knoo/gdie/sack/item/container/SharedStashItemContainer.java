package knoo.gdie.sack.item.container;

import knoo.gdie.file.FileReader.GDBinaryFileReader;
import knoo.gdie.sack.character.GDSharedStashSpecialCharacter;

import java.io.IOException;

/**
 * Created by knoodrake on 28/03/2017.
 */
public class SharedStashItemContainer extends AbstractItemContainer {

    private int width = 0;
    private int height = 0;

    public SharedStashItemContainer(GDBinaryFileReader reader, int pos) {
        super(reader, new GDSharedStashSpecialCharacter());
        this.pos = pos;
    }

    @Override
    public ItemContainerType getItemContainerType() {
        return ItemContainerType.SHARED_STASH;
    }

    @Override
    public GDBinaryFileReader getReader() {
        return reader;
    }

    @Override
    public void initReading() throws IOException {
        if(readBlockStartState !=0 )
            throw new IOException("Ünsuported version");

        width = getReader().rInt();
        height = getReader().rInt();
    }
}
