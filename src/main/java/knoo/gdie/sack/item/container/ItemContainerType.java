package knoo.gdie.sack.item.container;

public enum ItemContainerType {
    CHAR_INVENTORY,
    CHAR_EQUIPMENT,
    CHAR_STASH,
    SHARED_STASH,
    FORMULAS
    }