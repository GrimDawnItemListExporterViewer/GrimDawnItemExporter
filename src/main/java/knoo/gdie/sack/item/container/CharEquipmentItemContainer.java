package knoo.gdie.sack.item.container;

import knoo.gdie.file.FileReader.GDBinaryFileReader;
import knoo.gdie.sack.character.GDCharacter;
import knoo.gdie.sack.item.GDEquipmentItem;

import java.io.IOException;

/**
 * Created by knoodrake on 27/03/2017.
 */
public class CharEquipmentItemContainer extends AbstractItemContainer {

    public CharEquipmentItemContainer(GDBinaryFileReader reader, GDCharacter owner) {
        super(reader,owner);
        this.pos = 0;
    }

    @Override
    public ItemContainerType getItemContainerType() {
        return ItemContainerType.CHAR_EQUIPMENT;
    }

    @Override
    public GDBinaryFileReader getReader() {
        return reader;
    }

    @Override
    public void startRead() throws IOException {}

    @Override
    public void stopRead() throws IOException {}

    @Override
    public void initReading() throws IOException {
    }

    @Override
    public void readItems() throws IOException {
        readAllExceptWeapons();
        readWeapons();
    }

    private void readWeapons() throws IOException {

        GDEquipmentItem weapon1a = new GDEquipmentItem(getReader(), this, getOwner());
        GDEquipmentItem weapon1b = new GDEquipmentItem(getReader(), this, getOwner());
        GDEquipmentItem weapon2a = new GDEquipmentItem(getReader(), this, getOwner());
        GDEquipmentItem weapon2b = new GDEquipmentItem(getReader(), this, getOwner());

        getReader().rByte(); // alternate1
        if(weapon1a.read()) items.add(weapon1a);
        if(weapon1b.read()) items.add(weapon1b);

        getReader().rByte(); // alternate2
        if(weapon2a.read()) items.add(weapon2a);
        if(weapon2b.read()) items.add(weapon2b);
    }

    public void readAllExceptWeapons() throws IOException {
        getReader().rByte(); // useAlternate
        numOfItems = 12;
        for(int i=0; i<12; i++) {
            GDEquipmentItem equipmentItem = new GDEquipmentItem(getReader(), this, getOwner());
            if(equipmentItem.read()) items.add(equipmentItem);
        }
    }
}
