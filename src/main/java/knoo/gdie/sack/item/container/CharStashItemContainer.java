package knoo.gdie.sack.item.container;

import knoo.gdie.file.FileReader.GDBinaryFileReader;
import knoo.gdie.sack.character.GDCharacter;

import java.io.IOException;

/**
 * Created by knoodrake on 27/03/2017.
 */
public class CharStashItemContainer extends AbstractItemContainer {

    private int width = 0;
    private int height = 0;

    public CharStashItemContainer(GDBinaryFileReader reader, GDCharacter owner) {
        super(reader,owner);
        this.pos = 0;
    }

    @Override
    public ItemContainerType getItemContainerType() {
        return ItemContainerType.CHAR_STASH;
    }

    @Override
    public GDBinaryFileReader getReader() {
        return reader;
    }

    @Override
    public void initReading() throws IOException {
        if(readBlockStartState !=4 )
            throw new IOException("Ünsuported version");

        int persoStashVersion = reader.rInt();
        if (persoStashVersion != 5)
            throw new IOException("Ünsuported version");

        width = getReader().rInt();
        height = getReader().rInt();
    }
}
