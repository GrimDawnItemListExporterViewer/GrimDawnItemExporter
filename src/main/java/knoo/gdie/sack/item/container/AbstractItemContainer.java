package knoo.gdie.sack.item.container;

import knoo.gdie.file.FileReader.GDBinaryFileReader;
import knoo.gdie.sack.character.GDCharacter;
import knoo.gdie.sack.item.GDItem;
import knoo.gdie.sack.item.ItemRarity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by knoodrake on 28/03/2017.
 */
public abstract class AbstractItemContainer implements ItemContainer {

    protected final static String NL = System.getProperty("line.separator");

    protected GDBinaryFileReader reader;
    protected int pos = 0;
    protected List<GDItem> items = new ArrayList<>();
    protected GDBinaryFileReader.block block;
    protected int readBlockStartState = -1;
    protected int numOfItems = 0;
    protected GDCharacter owner = null;

    public AbstractItemContainer(GDBinaryFileReader reader, GDCharacter owner) {
       this.reader = reader;
        this.owner = owner;
    }

    @Override
    public List<GDItem> getItems() {
        return items;
    }

    @Override
    public List<GDItem> getItemsAtLeast(ItemRarity rarity) {
        return items.stream().filter(item ->
                item.getRarity().getRarityLevel() >= rarity.getRarityLevel()
        ).collect(Collectors.toList());
    }
    @Override
    public List<GDItem> getItemsThatAre(ItemRarity rarity) {
        return items.stream().filter(item ->
                        item.getRarity().equals(rarity)
        ).collect(Collectors.toList());
    }
//
    @Override
    public List<GDItem> getItemsThatAre(List<ItemRarity> rarities) {
        return items.stream().filter(item ->
                        rarities.contains(item.getRarity())
        ).collect(Collectors.toList());
    }

    @Override
    public int getPosition() {
        return pos;
    }


    public GDCharacter getOwner() {
        return owner;
    }

    public abstract ItemContainerType getItemContainerType();

    public abstract GDBinaryFileReader getReader();

    public int getNumOfItems() {
        return numOfItems;
    }

    public void startRead() throws IOException {
        block = new GDBinaryFileReader.block();
        readBlockStartState = getReader().rBlockStart(block);
    }

    public void stopRead() throws IOException {
        getReader().rBlockEnd(block);
    }

    public void read() throws IOException {
        startRead();
        initReading();
        readItems();
        stopRead();
    }

    public abstract void initReading() throws IOException;

    public void readItems() throws IOException {
        numOfItems = getReader().rInt();
        for (int i = 0; i < numOfItems; ++i) {
            GDItem gdItem = new GDItem(getReader(), this, getOwner());
            gdItem.read();
            if(gdItem.isOfInterest())
                items.add(gdItem);
        }
    }

    @Override
    public String toString() {
        return "Sack[" + getClass().getSimpleName() + "] pos:" + pos + " containing " + getNumOfItems() + " items";
    }

    public String toStringWithItems() {
        StringBuilder sb = new StringBuilder();
        sb.append(toString());
        sb.append(NL);
        for(GDItem item : items) {
            if(item.isOfInterest() && item.getRarity().getRarityLevel() > ItemRarity.EPIC.getRarityLevel()) {
                sb.append(item.toString());
            }
        }
        return sb.toString();
    }

    @Override
    public void clear() {
        commitSuicide();
        items.forEach(i -> i.commitSuicide());
        items.clear();
    }

    // TODO just a test for memleaks
    protected void commitSuicide() {
        this.reader = null;
        this.owner = null;
        this.block = null;
    }
}
