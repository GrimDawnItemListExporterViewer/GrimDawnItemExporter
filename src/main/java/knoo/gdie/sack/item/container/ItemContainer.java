package knoo.gdie.sack.item.container;

import knoo.gdie.sack.item.GDItem;
import knoo.gdie.sack.item.ItemRarity;

import java.util.List;

/**
 * Created by knoodrake on 29/03/2017.
 */
public interface ItemContainer {

    int getNumOfItems();

    ItemContainerType getItemContainerType();

    List<GDItem> getItems();

    List<GDItem> getItemsAtLeast(ItemRarity rarity);

    List<GDItem> getItemsThatAre(ItemRarity rarity);

    List<GDItem> getItemsThatAre(List<ItemRarity> rarities);

    String toStringWithItems();

    int getPosition();

    void clear();
}
