package knoo.gdie.sack.item.container;

import knoo.gdie.file.FileReader.GDBinaryFileReader;
import knoo.gdie.sack.character.GDCharacter;

import java.io.IOException;

/**
 * Created by knoodrake on 27/03/2017.
 */
public class CharInventoryItemContainer extends AbstractItemContainer {

    public CharInventoryItemContainer(GDBinaryFileReader reader, int pos, GDCharacter owner) {
        super(reader,owner);
        this.pos = pos;
    }

    @Override
    public ItemContainerType getItemContainerType() {
        return ItemContainerType.CHAR_INVENTORY;
    }

    @Override
    public GDBinaryFileReader getReader() {
        return reader;
    }

    @Override
    public void initReading() throws IOException {
        if(readBlockStartState !=0 )
            throw new IOException("Ünsuported version");

        reader.rByte(); // ??? tmp bool

    }
}
