package knoo.gdie.sack.item.gear;

import knoo.gdie.log.GDIELogger;
import knoo.gdie.recordsdb.DBFactory;
import knoo.gdie.sack.character.GDFormulasSpecialCharacter;
import knoo.gdie.sack.character.GDSharedStashSpecialCharacter;
import knoo.gdie.sack.item.GDItem;
import knoo.gdie.sack.item.container.ItemContainer;
import knoo.gdie.sack.item.container.ItemContainerType;

import java.io.IOException;

/**
 * Created by knoodrake on 08/04/2017.
 */
public class GDFormulaItem extends GDItem {

    public GDFormulaItem(String recordStr, ItemContainer container) {
        super(null, container, new GDFormulasSpecialCharacter());
        setItemID(recordStr);
        fillBlankField();
    }

    private void fillBlankField() {
    }

    @Override
    public boolean read() throws IOException {
        this.x = 0;
        this.y = 0;

        if(itemID.isEmpty()) {
            // NO ITEM ( Ex: 2nd Weapon when wearing 2H weapon, or unequiped place on char )
            completeNiceName = "! empty slot";
            return false;
        }
        if(!itemID.startsWith("records/items/")) {
            completeNiceName = "! unsupported item";
            return false;
        }

        dbrProperties = DBFactory.getDB().get(itemID);
        analyzeDBRItemProperties();

        gearType = GearType.LEARNED_BLUEPRINTS;
        return true;
    }

    public boolean isJuggedDuplicate() {
        return true;
    }

}
