package knoo.gdie.sack.item.gear;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum GearType {
    GEAR(null),
        BLUEPRINTS(GEAR),
            LEARNED_BLUEPRINTS(BLUEPRINTS),
            BLUEPRINT_SCROLL(BLUEPRINTS),
        ACCESSORIES(GEAR),
            MEDALS(ACCESSORIES),
            NECKLACES(ACCESSORIES),
            RINGS(ACCESSORIES),
            RELIC(ACCESSORIES),
        ARMOR(GEAR),
            BELT(ARMOR),
            FEET(ARMOR),
            HANDS(ARMOR),
            HEAD(ARMOR),
            LEGS(ARMOR),
            SHOULDER(ARMOR),
            TORSO(ARMOR),
        WEAPON(GEAR),
            MELEE1H(WEAPON),
            RANGED1H(WEAPON),
            OFFHAND(WEAPON),
            SHIELD(WEAPON),
            MELEE2H(WEAPON),
            RANGED2H(WEAPON),
        UNKNOWN(GEAR)
        ;

    private GearType parent = null;
    protected List<GearType> children = new ArrayList<GearType>();

    private GearType(GearType parent) {
        this.parent = parent;
        if (this.parent != null) {
            this.parent.children.add(this);
        }
    }

    public List<GearType> getSubtypes() {
        return Arrays.asList(children.toArray(new GearType[children.size()]));
    }

    public boolean isA(GearType other) {
        if (other == null) {
            return false;
        }

        for (GearType t = this;  t != null;  t = t.parent) {
            if (other == t) {
                return true;
            }
        }
        return false;
    }

    public GearType getParent() {
        if(parent == null)
            return UNKNOWN;
        return parent;
    }
}