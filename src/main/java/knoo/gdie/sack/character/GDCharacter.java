package knoo.gdie.sack.character;

/**
 * Created by knoodrake on 28/03/2017.
 */
public class GDCharacter {

    public enum Sexe {
        MALE, FEMALE;
    }

    public enum MainDifficulty {
        NORMAL, VETERAN, ELITE, ULTIMATE;
    }


    public enum CrucibleDifficulty {
        ASPIRANT, CHALLENGER, GLADIATOR;
    }
    protected String name;
    protected boolean hasBeenInGame;
    protected Sexe sex;
    protected String classID;
    protected int level;
    protected boolean isHardcore;
    protected int version;
    protected String uid;
    protected boolean hasCrucible;
    protected MainDifficulty mainDifficulty;
    protected int money;
    protected CrucibleDifficulty crucibleDifficulty = null;
    protected int tributePoints = 0;
    protected int xp;
    protected int attributePoints;
    protected int skillPoints;
    protected int devotionPoints;
    protected int totalDevotion;
    protected int physique;
    protected int cunning;
    protected int spirit;
    protected int health;
    protected int energy;
    protected int numberOfInventorySacks;

    public int getCunning() {
        return cunning;
    }

    public void setCunning(int cunning) {
        this.cunning = cunning;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sexe getSex() {
        return sex;
    }

    public void setSex(byte sex) {
        this.sex = (sex == 1 ? Sexe.MALE : Sexe.FEMALE);
    }

    public String getClassID() {
        return classID;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isHardcore() {
        return isHardcore;
    }

    public void setHardcore(byte isHardcore) {
        this.isHardcore = (isHardcore == 1 ? true : false);
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public boolean hasCrucible() {
        return hasCrucible;
    }

    public void setHasCrucible(int hasCrucible) {
        this.hasCrucible = (hasCrucible == 4 ? true : false);
    }

    public MainDifficulty getMainDifficulty() {
        return mainDifficulty;
    }

    public void setMainDifficulty(byte mainDifficulty) {
        switch (mainDifficulty) {
            case 0:
                this.mainDifficulty = MainDifficulty.NORMAL;
                break;
            case 1:
                this.mainDifficulty = MainDifficulty.VETERAN;
                break;
            case 2:
                this.mainDifficulty = MainDifficulty.ELITE;
                break;
            case 3:
                this.mainDifficulty = MainDifficulty.ULTIMATE;
                break;
        }
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public CrucibleDifficulty getCrucibleDifficulty() {
        return crucibleDifficulty;
    }

    public void setCrucibleDifficulty(byte crucibleDifficulty) {
        switch (crucibleDifficulty) {
            case 0:
                this.crucibleDifficulty = CrucibleDifficulty.ASPIRANT;
                break;
            case 1:
                this.crucibleDifficulty = CrucibleDifficulty.CHALLENGER;
                break;
            case 2:
                this.crucibleDifficulty = CrucibleDifficulty.GLADIATOR;
                break;
        }
    }


    public void setHasBeenInGame(byte b) {
        hasBeenInGame = (b == 1);
    }

    public boolean hasBeenInGame() {
        return hasBeenInGame;
    }

    public int getTributePoints() {
        return tributePoints;
    }

    public void setTributePoints(int tributePoints) {
        this.tributePoints = tributePoints;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public int getAttributePoints() {
        return attributePoints;
    }

    public void setAttributePoints(int attributePoints) {
        this.attributePoints = attributePoints;
    }

    public int getSkillPoints() {
        return skillPoints;
    }

    public void setSkillPoints(int skillPoints) {
        this.skillPoints = skillPoints;
    }

    public int getDevotionPoints() {
        return devotionPoints;
    }

    public void setDevotionPoints(int devotionPoints) {
        this.devotionPoints = devotionPoints;
    }

    public int getTotalDevotion() {
        return totalDevotion;
    }

    public void setTotalDevotion(int totalDevotion) {
        this.totalDevotion = totalDevotion;
    }

    public int getPhysique() {
        return physique;
    }

    public void setPhysique(int physique) {
        this.physique = physique;
    }

    public int getSpirit() {
        return spirit;
    }

    public void setSpirit(int spirit) {
        this.spirit = spirit;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public int getNumberOfInventorySacks() {
        return numberOfInventorySacks;
    }

    public void setNumberOfInventorySacks(int numberOfInventorySacks) {
        this.numberOfInventorySacks = numberOfInventorySacks;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("[ ");
        sb.append("name:" + getName() + ", ");
        sb.append("sex:" + getSex() + ", ");
        sb.append("classID:" + getClassID() + ", ");
        sb.append("level:" + getLevel() + ", ");
        sb.append("isHardcore:" + isHardcore() + ", ");
        sb.append("version:" + getVersion() + ", ");
        sb.append("uid:" + getUid() + ", ");
        sb.append("hasCrucible:" + hasCrucible() + ", ");
        sb.append("mainDifficulty:" + getMainDifficulty() + ", ");
        sb.append("crucibleDifficulty:" + getCrucibleDifficulty() + ", ");
        sb.append("money:" + getMoney() + ", ");
        sb.append("tributes:" + getTributePoints() + ", ");
        sb.append("xp:" + getXp() + ", ");
        sb.append("attributePts:" + getAttributePoints() + ", ");
        sb.append("skillPts:" + getSkillPoints() + ", ");
        sb.append("devotionPts:" + getDevotionPoints() + ", ");
        sb.append("totalDevotion:" + getTotalDevotion() + ", ");
        sb.append("physique:" + getPhysique() + ", ");
        sb.append("cunning:" + getCunning() + ", ");
        sb.append("spirit:" + getSpirit() + ", ");
        sb.append("health:" + getHealth() + ", ");
        sb.append("energy:" + getEnergy() + ", ");
        sb.append("inventoryBags:" + getNumberOfInventorySacks() + ", ");
        sb.append(" ]");

        return sb.toString();
    }
}
