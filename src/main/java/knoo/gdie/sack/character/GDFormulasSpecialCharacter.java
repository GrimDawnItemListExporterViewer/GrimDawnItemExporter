package knoo.gdie.sack.character;

/**
 * Created by knoodrake on 03/04/2017.
 */
public class GDFormulasSpecialCharacter extends GDCharacter {

    public GDFormulasSpecialCharacter() {
        name = "Shared";
        hasBeenInGame = false;
        sex = Sexe.FEMALE;
        classID = "LEARNED";
        level = 1;
        isHardcore = false;
        version = 0;
        uid = "LEARNED";
        hasCrucible = false;
        mainDifficulty = MainDifficulty.NORMAL;
        money = 0;
        crucibleDifficulty = CrucibleDifficulty.ASPIRANT;
        tributePoints = 0;
        xp = 0;
        attributePoints = 0;
        skillPoints = 0;
        devotionPoints = 0;
        totalDevotion = 0;
        physique = 0;
        cunning = 0;
        spirit = 0;
        health = 0;
        energy = 0;
        numberOfInventorySacks = 0;    }
}
