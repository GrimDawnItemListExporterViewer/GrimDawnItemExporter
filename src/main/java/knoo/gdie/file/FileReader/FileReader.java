package knoo.gdie.file.FileReader;

import java.io.IOException;

/**
 * Created by knoodrake on 08/04/2017.
 */
public interface FileReader {

    public void readFileIntoBuffer(String filename) throws IOException;

}
