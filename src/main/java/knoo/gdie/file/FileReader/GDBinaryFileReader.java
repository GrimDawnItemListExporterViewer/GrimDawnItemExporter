package knoo.gdie.file.FileReader;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by knoodrake on 27/03/2017.
 */
public class GDBinaryFileReader implements FileReader {
    public int pos;
    public int key;
    public int[] table = new int[256];
    public byte[] buf = new byte[32];

    @Override
    public void readFileIntoBuffer(String filename) throws IOException {
        FileInputStream file = new FileInputStream(filename);
        buf = new byte[(int)file.getChannel().size()];
        pos = 0;
        file.read(buf);
        file.close();
    }

    static public class block
    {
        public int len;
        public int end;
    }


    public void rKey() throws IOException
    {
        int k = get();
        k |= get() << 8;
        k |= get() << 16;
        k |= get() << 24;

        k ^= 0x55555555;

        key = k;

        for (int i = 0; i < 256; i++)
        {
            k = Integer.rotateRight(k, 1);
            k *= 39916801;
            table[i] = k;
        }
    }

    public int rBlockStart(block b) throws IOException
    {
        int ret = rInt();

        b.len = nextInt();
        b.end = pos + b.len;

        return ret;
    }
    public String rWideStr() throws IOException {
        int len = rInt();
        if (len == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder(len);
        len = 2 * len;
        for (int i = 0; i < len; i += 2) {
            byte b1 = rByte();
            byte b2 = rByte();
            short s = b2;
            s = (short)(s << 8);
            char c = (char)(b1 | s);
            sb.append(c);
        }
        String s = sb.toString();
        return s;
    }

    public int get() throws IOException
    {
        if (pos >= buf.length)
            throw new IOException();

        return buf[pos++] & 0xFF;
    }
    public String rStr() throws IOException
    {
        int len = rInt();

        StringBuilder sb = new StringBuilder(len);

        for (int i = 0; i < len; i++)
        {
            sb.append((char) rByte());
        }

        return sb.toString();
    }


    public int nextInt() throws IOException
    {
        int ret = get();
        ret |= get() << 8;
        ret |= get() << 16;
        ret |= get() << 24;

        ret ^= key;

        return ret;
    }

    public int rInt() throws IOException
    {
        int k = key;

        int val = get();
        k ^= table[val];

        int b = get();
        k ^= table[b];
        val |= b << 8;

        b = get();
        k ^= table[b];
        val |= b << 16;

        b = get();
        k ^= table[b];
        val |= b << 24;

        val ^= key;

        key = k;

        return val;
    }

    public byte rByte() throws IOException
    {
        int b = get();
        int ret = (b ^ key) & 0xFF;
        key ^= table[b];
        return (byte) ret;
    }

    public float rFloat() throws IOException
    {
        return Float.intBitsToFloat(rInt());
    }


    public void rBlockEnd(block b) throws IOException
    {
        if (pos != b.end)
            throw new IOException();

        if (nextInt() != 0)
            throw new IOException();
    }
}
