package knoo.gdie.file.FileReader;

import sun.nio.cs.US_ASCII;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by knoodrake on 08/04/2017.
 */
public class GDFormulaFileReader implements FileReader {


    private String buffer = null;
    private static final String NUM_ENTRIES = "numEntries";
    private static final String MSDOS_LATIN1_CHARSET = "IBM850";

    @Override
    public void readFileIntoBuffer(String filename) throws IOException {
        byte[] bytes = Files.readAllBytes(Paths.get(filename));
        buffer = new String(bytes, MSDOS_LATIN1_CHARSET);
    }

    public int getNumOfEntries() {
        int numOfEntriesIdx = buffer.indexOf(NUM_ENTRIES);
        int start = numOfEntriesIdx + NUM_ENTRIES.length();
        String hexNumOfEntries = buffer.substring(start, start + 1);
        int returnVal = 0;
        byte[] bytes = null;
        try {
            bytes = hexNumOfEntries.getBytes(MSDOS_LATIN1_CHARSET);
            int i = bytes[0] & 0xFF;
            returnVal = i;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return returnVal;
    }

    public List<String> read() {
        int numOfEntries = getNumOfEntries();
        final String recordStr = "records/";
        final int recordStrLen = recordStr.length();
        List<String> records = new ArrayList<>();
        int currentStartInx = 0;
        for(int i=0; i<numOfEntries; i++) {
            int entryStartIndex = buffer.indexOf(recordStr, currentStartInx);
            int entryEndIndex = buffer.indexOf(".dbr", entryStartIndex + recordStrLen);
            records.add(buffer.substring(entryStartIndex, entryEndIndex + 4));
            currentStartInx = entryEndIndex + 4;
        }
        return records;
    }
}
