package knoo.gdie.file.decryptor;


import knoo.gdie.file.FileReader.GDBinaryFileReader;
import knoo.gdie.file.FileReader.GDFormulaFileReader;
import knoo.gdie.sack.item.container.FormulaStashContainer;
import knoo.gdie.sack.item.container.ItemContainer;
import knoo.gdie.sack.item.container.SharedStashItemContainer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GSTFormulasDecoder implements GDDecoder, ItemContainerRepository
{
    private GDFormulaFileReader reader;
    private FormulaStashContainer formulas = null;

    public GSTFormulasDecoder(GDFormulaFileReader reader) {
        this.reader = reader;
    }

    @Override
    public void decode(String filename) throws IOException
    {
        reader.readFileIntoBuffer(filename);
        List<String> records = reader.read();
        formulas = new FormulaStashContainer();
        formulas.addRecords(records);
    }

    @Override
    public List<ItemContainer> getItemContainers() {
        ArrayList<ItemContainer> formulaContainers = new ArrayList<>();
        formulaContainers.add(formulas);
        return formulaContainers;
    }

    @Override
    public void clear(boolean propagateClearing) {
        if(formulas != null)
            formulas.clear();
    }

}