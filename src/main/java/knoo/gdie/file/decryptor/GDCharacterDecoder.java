package knoo.gdie.file.decryptor;

import knoo.gdie.sack.character.GDCharacter;

/**
 * Created by knoodrake on 29/03/2017.
 */
public interface GDCharacterDecoder
{

    public GDCharacter getGDCharacter();

}
