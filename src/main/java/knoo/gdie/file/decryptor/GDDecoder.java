package knoo.gdie.file.decryptor;

import java.io.IOException;

/**
 * Created by knoodrake on 28/03/2017.
 */
public interface GDDecoder {

    public void decode(String filename) throws IOException;

}
