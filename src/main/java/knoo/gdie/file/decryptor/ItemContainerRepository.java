package knoo.gdie.file.decryptor;

import knoo.gdie.sack.item.container.ItemContainer;

import java.util.List;

/**
 * Created by knoodrake on 29/03/2017.
 */
public interface ItemContainerRepository {

    public List<ItemContainer> getItemContainers();

    public void clear(boolean propagateClearing);

}
