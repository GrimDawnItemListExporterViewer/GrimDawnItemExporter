package knoo.gdie.file.decryptor;

import knoo.gdie.Main;
import knoo.gdie.file.FileReader.GDBinaryFileReader;
import knoo.gdie.file.FileReader.GDBinaryFileReader.block;
import knoo.gdie.log.GDIELogger;
import knoo.gdie.sack.character.GDCharacter;
import knoo.gdie.sack.item.container.CharEquipmentItemContainer;
import knoo.gdie.sack.item.container.CharInventoryItemContainer;
import knoo.gdie.sack.item.container.CharStashItemContainer;
import knoo.gdie.sack.item.container.ItemContainer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by knoodrake on 27/03/2017.
 */

public class GDCDecoder implements GDDecoder, ItemContainerRepository, GDCharacterDecoder
{

    private GDBinaryFileReader r;
    private List<ItemContainer> itemContainers = new ArrayList<>();
    private GDCharacter gdCharacter;

    public GDCDecoder(GDBinaryFileReader r) {
            this.r = r;
    }

    @Override
    public void decode(String filename) throws IOException
    {
        r.readFileIntoBuffer(filename);

        r.rKey();
        if (r.rInt() != 1480803399)
            throw new IOException("Ünsuported version");
        if (r.rInt() != 1)
            throw new IOException("Ünsuported version");

        gdCharacter = new GDCharacter();

        // Char Header
        gdCharacter.setName(r.rWideStr());
        gdCharacter.setSex(r.rByte());
        gdCharacter.setClassID(r.rStr());
        gdCharacter.setLevel(r.rInt());
        gdCharacter.setHardcore(r.rByte());

        r.nextInt();

        // should be 6 or 7 ??
        gdCharacter.setVersion(r.rInt());

        // Char UID
        byte[] uid = new byte[16];
        for (int i = 0; i < uid.length; ++i) {
            uid[i] = r.rByte();
        }
        gdCharacter.setUid(uid.toString());

        // Char basic INFOS ( summary )
        GDBinaryFileReader.block b = new GDBinaryFileReader.block();
        if(r.rBlockStart(b) != 1)
            throw new IOException("Ünsuported version");

        int version = r.rInt();
        if (version != 4 && version != 3)
            throw new IOException("Ünsuported version");
        gdCharacter.setHasCrucible(version);

        r.rByte(); // isInMainQuest
        gdCharacter.setHasBeenInGame(r.rByte());
        r.rByte(); // currentDifficulty

        gdCharacter.setMainDifficulty(r.rByte());
        gdCharacter.setMoney(r.rInt());

        if(gdCharacter.hasCrucible()) {
            gdCharacter.setCrucibleDifficulty(r.rByte());
            gdCharacter.setTributePoints(r.rInt());
        }

        r.rByte(); // compassState
        r.rInt(); // lootMode
        r.rByte(); // skillWindowHelp
        r.rByte(); // alternateConfig
        r.rByte(); // alternateConfigEnabled
        r.rStr(); // texture

        r.rBlockEnd(b);

        // Char BIO

        b = new block();
        if(r.rBlockStart(b) != 2)
            throw new IOException("Ünsuported version");

        int bioVersion = r.rInt();
        if (bioVersion != 8)
            throw new IOException("Ünsuported version");

        r.rInt(); // level (again)

        gdCharacter.setXp(r.rInt());
        gdCharacter.setAttributePoints(r.rInt());
        gdCharacter.setSkillPoints(r.rInt());
        gdCharacter.setDevotionPoints(r.rInt());
        gdCharacter.setTotalDevotion(r.rInt());
        gdCharacter.setPhysique((int) r.rFloat());
        gdCharacter.setCunning((int) r.rFloat());
        gdCharacter.setSpirit((int) r.rFloat());
        gdCharacter.setHealth((int) r.rFloat());
        gdCharacter.setEnergy((int) r.rFloat());

        r.rBlockEnd(b);

        if(gdCharacter.hasBeenInGame()) {
            readInventoryAndEquipment();
            readPersonalStash();
        }

        // TODO ( ou pas )
//            respawns.read();
//            teleports.read();
//            markers.read();
//            shrines.read();
//            skills.read();
//            notes.read();
//            factions.read();
//            ui.read();
//            tutorials.read();
//            stats.read();
//            if (version == 7) {
//                crucible.read();
//            }
    }

    private void readPersonalStash() throws IOException {
        CharStashItemContainer personalStash = new CharStashItemContainer(r, gdCharacter);
        personalStash.read();
        itemContainers.add(personalStash);
    }

    private void readInventoryAndEquipment() throws IOException {
        block b;
        b = new block(); // bcz fuck mem leak
        if(r.rBlockStart(b) != 3)
            throw new IOException("Ünsuported version");

        int invVersion = r.rInt();
        if (invVersion != 4)
            throw new IOException("Ünsuported version");

        readInventory();
        readEquipment();

        r.rBlockEnd(b);
    }

    private void readEquipment() throws IOException {
        // Char EQUIPMENT
        CharEquipmentItemContainer equipment = new CharEquipmentItemContainer(r, gdCharacter);
        equipment.read();
        itemContainers.add(equipment);
    }

    private void readInventory() throws IOException {
        byte flag = r.rByte();
        if (flag != 0) {
            int numSacks = r.rInt();
            gdCharacter.setNumberOfInventorySacks(numSacks);

            r.rInt(); // focused
            r.rInt(); // selected

            for (int i = 0; i < numSacks; ++i) {
                CharInventoryItemContainer sack = new CharInventoryItemContainer(r, i, gdCharacter);
                sack.read();
                itemContainers.add(sack);
            }
        }
    }

    @Override
    public List<ItemContainer> getItemContainers() {
        return itemContainers;
    }

    @Override
    public void clear(boolean propagateClearing) {
        if(propagateClearing)
            itemContainers.forEach(container -> container.clear());
        itemContainers.clear();
        gdCharacter = null;
    }

    @Override
    public GDCharacter getGDCharacter() {
        return gdCharacter;
    }
}
