package knoo.gdie.file.decryptor;


import knoo.gdie.file.FileReader.GDBinaryFileReader;
import knoo.gdie.sack.item.container.ItemContainer;
import knoo.gdie.sack.item.container.SharedStashItemContainer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GSTDecoder implements GDDecoder, ItemContainerRepository
{
    String mod;

    private GDBinaryFileReader reader;
    private List<ItemContainer> itemContainers = new ArrayList<>();

    public GSTDecoder(GDBinaryFileReader reader) {
        this.reader = reader;
    }

    @Override
    public void decode(String filename) throws IOException
    {
        reader.readFileIntoBuffer(filename);
        reader.rKey();

        if (reader.rInt() != 2)
            throw new IOException();

        GDBinaryFileReader.block b = new GDBinaryFileReader.block();

        if (reader.rBlockStart(b) != 18)
            throw new IOException();

        if (reader.rInt() != 4) // version
            throw new IOException();

        if (reader.nextInt() != 0)
            throw new IOException();

        mod = reader.rStr();
        int numSacks = reader.rInt();

        for (int i = 0; i < numSacks; ++i) {
            SharedStashItemContainer sack = new SharedStashItemContainer(reader, i);
            sack.read();
            itemContainers.add(sack);
        }

        reader.rBlockEnd(b);
    }

    @Override
    public List<ItemContainer> getItemContainers() {
        return itemContainers;
    }

    @Override
    public void clear(boolean propagateClearing) {
        if(propagateClearing)
            itemContainers.forEach(container -> container.clear());
        itemContainers.clear();
    }

}