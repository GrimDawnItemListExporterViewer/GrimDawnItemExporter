package knoo.gdie;

import knoo.gdie.log.GDIELogger;
import knoo.gdie.sack.item.GDItem;
import knoo.gdie.sack.item.filters.Filters;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by knoodrake on 03/04/2017.
 */
public class GameFilesFilter {

    public List<GDItem> filter(List<GDItem> allItems) {

        final List<GDItem> filteredLegendaries = allItems.stream()
                .parallel()
                .filter(Filters.onlyThoseOfInterest())
                .filter(Filters.matchesSkipItemsSearch())
                .filter(Filters.skippedItems())
                .filter(Filters.skippedSharedStash())
                .filter(Filters.skippedNotSetLegendaries())
                .filter(Filters.skippedNotSetEpics())
                .filter(Filters.ownedBySkippedCharacters())
                .filter(Filters.outputBlueprintsAndRecipies())
                .filter(Filters.byRarity())
                .filter(Filters.noArtifacts()) //TODO Currently artifacts are just not handled
                .filter(Filters.skippedCharacterStash())
                .filter(Filters.skippedCharacterInventory())
                .filter(Filters.skippedEquippedItems())
                .filter(Filters.skippedSharedStashTab())
                .filter(Filters.skippedCharacterInventoryTab())
                .filter(Filters.customFilter())
                .collect(Collectors.toList());

        if(filteredLegendaries.size() == 0) {
            GDIELogger.get().logErr("Nothing to output, try to filter less stuff");
        }

        return filteredLegendaries;
    }
}
