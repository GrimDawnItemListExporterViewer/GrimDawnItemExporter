package knoo.gdie.ui.directoryChooser;

import com.dooapp.fxform.view.FXFormNode;
import com.dooapp.fxform.view.FXFormNodeWrapper;
import javafx.util.Callback;

public class DirectoryFieldFactory implements Callback<Void, FXFormNode> {

        public FXFormNode call(Void aVoid) {
            final DirectoryChooserWidget directoryChooserWidget = new DirectoryChooserWidget();
            return new FXFormNodeWrapper(directoryChooserWidget, directoryChooserWidget.textProperty());
        }
    }