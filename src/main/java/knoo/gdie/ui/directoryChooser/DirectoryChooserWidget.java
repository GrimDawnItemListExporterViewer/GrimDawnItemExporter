package knoo.gdie.ui.directoryChooser;

import javafx.beans.property.StringProperty;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.DirectoryChooser;
import knoo.gdie.log.GDIELogger;

import java.io.File;


public class DirectoryChooserWidget extends HBox {

        private TextField textField;
        private Button openDialogBtn;
        private DirectoryChooser directoryChooser;

        public DirectoryChooserWidget() {
            this.textField = new TextField();
            this.openDialogBtn = new Button("Choose");
            this.directoryChooser = new DirectoryChooser();
            getChildren().add(textField);
            getChildren().add(openDialogBtn);
            this.textField.setPrefWidth(400);

            openDialogBtn.setOnMouseClicked(event -> {
                if(textField.textProperty() != null && textField.textProperty().get() != null) {
                    final File value = new File(textField.textProperty().get());
                    if(value != null && value.exists()) {
                        try {
                            directoryChooser.setInitialDirectory(value);
                        } catch (Exception e) {
                            GDIELogger.get().log(e);
                        }
                    }
                }
                File file = directoryChooser.showDialog(openDialogBtn.getScene().getWindow());
                if(file != null)
                    this.textField.textProperty().setValue(file.getAbsolutePath());
            });
        }

        public StringProperty textProperty() {
            return textField.textProperty();
        }
    }