package knoo.gdie.ui.webview;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import knoo.gdie.Main;
import knoo.gdie.config.OutputFormat;
import knoo.gdie.log.GDIELogger;
import org.kordamp.ikonli.Ikon;
import org.kordamp.ikonli.javafx.FontIcon;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * Created by knoodrake on 18/04/2017.
 */
public class WebviewPanel extends VBox{

    private WebView browser = null;
    private HBox toolbar = null;
    private Button copyBtn;
    private Button saveAsBtn;
    private FileChooser fileChooser;

    private String outputedContent = null;
    private OutputFormat outputedContentFormat = null;

    public WebviewPanel() {
        this.browser = new WebView();
        this.toolbar = new HBox();
        this.fileChooser = new FileChooser();
        getChildren().add(toolbar);
        getChildren().add(browser);
        buildToolbar();
    }

    private void buildToolbar() {
        copyBtn = new Button("Copy content to clipboard", new FontIcon("fa-clipboard"));
        saveAsBtn = new Button("save to file", new FontIcon("fa-save"));
        toolbar.getChildren().add(saveAsBtn);
        toolbar.getChildren().add(copyBtn);

        copyBtn.setOnMouseClicked(event -> {
            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            StringSelection contents = new StringSelection(outputedContent);
            clipboard.setContents(contents,contents);

            Alert alert = new Alert(Alert.AlertType.INFORMATION, "Content copied to clipboard", ButtonType.OK);
            alert.showAndWait();
        });


        saveAsBtn.setOnMouseClicked(event -> {
            fileChooser.setInitialFileName("grimdawn-items-list" + outputedContentFormat.getExtension());
            FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter(outputedContentFormat.getFormatName(), outputedContentFormat.getExtension());
            fileChooser.setSelectedExtensionFilter(filter);
            File file = fileChooser.showSaveDialog(saveAsBtn.getScene().getWindow());
            try {
                if(file.canWrite() || file.createNewFile()) {
                    try(  PrintWriter out = new PrintWriter(file.getAbsolutePath())  ){
                        out.println( outputedContent );
                        Alert alert = new Alert(Alert.AlertType.INFORMATION, "file saved to: " + file.getAbsolutePath(), ButtonType.OK);
                        alert.showAndWait();
                        GDIELogger.get().log("file saved to: " + file.getAbsolutePath());
                    }
                } else {
                    GDIELogger.get().logErr("Could not save file: no permission");
                }
            } catch (IOException e) {
                GDIELogger.get().log(e);
            }
        });
    }

    public void loadContent(String contentStr) {
        OutputFormat outputFormat = OutputFormat.getFromName(Main.getConfig().getSectionMainSettings().getOutputFormat().toLowerCase());
        String contentType;
        switch (outputFormat) {
            case BBCODE:
            case TEXT:
            case JSON:
            case CSV:
                contentType = "text/plain";
                copyBtn.setVisible(true);
                break;
            case JSHTML:
            case HTML:
            default:
                contentType = "text/html";
                copyBtn.setVisible(false);
        }
        outputedContent = contentStr;
        outputedContentFormat = outputFormat;
        browser.getEngine().loadContent(contentStr, contentType);
    }
}
